"""
Chris Lee with help from Graeme Best's code

"""

import time, sys
import tqdm
import yaml
import os
import pprint
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import multiprocessing as mp

from datetime import date, datetime
from mcts import mcts, multi_mcts, multi_mcts_real_data
from action import Action, MultiRobotActions, printActionSequence, toActionMultiActionSequence
from plot_tree import plotTree
from ssap import SSAP_Poisson
from reward import multi_ssap_reward
from functools import partial

from multiprocessing.pool import ThreadPool
from contextlib import contextmanager


np.set_printoptions(linewidth=300)

def real_data_multi_comparison_run():
    """
    Compare SSAP vs MCTS

    """
    # Load config
    config = load_config(cfg='config/real_data_multi_ssap_config.yaml')

    # Test params
    test_params = config['test_params']
    comparison_iterations = test_params['comparison_iterations']
    total_deploy = test_params['total_deploy']
    total_ground_robots = test_params['total_ground_robots']
    total_stages = test_params['total_stages']
    total_experiments = test_params['algorithms']
    count_data_loc = test_params['counts_data_loc']
    overlaps_loc = test_params['overlaps_loc']

    # SSAP class
    ssap_params = config['ssap_params']
    poisson_mu = ssap_params['poisson_mu']
    ssap = SSAP_Poisson(total_stages, poisson_mu)
    ssap.total_drone_deploys = total_deploy

    # MCTS params
    mcts_params = config['mcts_params']

    total_ssap_rewards = []
    total_mcts_rand_rewards = []
    total_mcts_ssap_rewards = []
    total_random_rewards = []

    # Saveout array
    save_action_arr = np.zeros((len(total_experiments),comparison_iterations, total_ground_robots, total_stages))
    save_obs_arr = np.zeros((comparison_iterations, total_ground_robots, total_stages))
    save_path = None

    # count data. assume comes in rounded
    count_data = np.load(count_data_loc)
    overlaps = np.load(overlaps_loc)
    overlaps = [tuple(vals) for vals in overlaps]
    print('SETTING OVERLAPS TO []')
    overlaps = []
    # print('Overlaps: ', overlaps)
    # do comparison iterations number of SSAP vs MCTS
    for comp_iter in range(comparison_iterations):
        multi_rob_obs = [counts for counts in count_data]
        # print('Obs: ', multi_rob_obs)
        total_oracle_reward = 1.0

        oracle_reward = multi_oracle_reward(multi_rob_obs, total_deploy)
        print('Oracle Reward: ', oracle_reward)
        # # Do SSAP 
        ssap_actions = multi_ssap_run(multi_rob_obs, oracle_reward, total_ground_robots, 
                                    total_deploy, total_stages, ssap, overlaps)
        ssap_reward = calc_penalty_reward(multi_rob_obs, overlaps, ssap_actions, 
                                            total_stages, total_ground_robots, total_deploy, ssap)     
        total_ssap_rewards.append(ssap_reward/float(oracle_reward))

        # Do MCTS - Random Rollout
        mcts_rand_actions = multi_mcts_run(multi_rob_obs, oracle_reward, total_ground_robots,
                                         total_deploy, total_stages, ssap, overlaps, mcts_params)
        mcts_rand_reward = calc_penalty_reward(multi_rob_obs, overlaps, mcts_rand_actions, 
                                            total_stages, total_ground_robots, total_deploy, ssap)
        total_mcts_rand_rewards.append(mcts_rand_reward/float(oracle_reward))

        
        # save
        save_action_arr[0,comp_iter] = ssap_actions
        save_action_arr[1,comp_iter] = mcts_rand_actions
        save_obs_arr[comp_iter] = np.array(multi_rob_obs)
        save_path = save_data(save_action_arr, save_obs_arr, config, save_path)

    fig, ax = plt.subplots()
    ax.boxplot([total_ssap_rewards, total_mcts_rand_rewards])
    ax.set(xticklabels=total_experiments)
    plt.savefig(save_path + 'comparison.eps', format='eps')

# ======== Misc Functions ========

def save_data(save_arr, obs_arr, config, save_path):
    """
    Saves out the results with hopefully all the config information

    """
    today = date.today()
    curr_date = today.strftime("%b-%d-%Y")
    # make it if it doesn't exist
    path = 'test_results/' + curr_date
    if not os.path.exists(path):
        os.makedirs(path)
    
    # first loop through
    if not save_path:
        # create current test path
        now = datetime.now()
        time_str = now.strftime("%H:%M:%S")
        save_path = path + '/' + time_str + '/'
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        # save config
        with open(save_path+'config.yaml','w') as outfile:
            yaml.dump(config, outfile)
    
    with open(save_path + 'results.npy', 'wb') as f:
        np.save(f, save_arr)

    with open(save_path + 'obs.npy', 'wb') as f:
        np.save(f, obs_arr)

    return save_path

def load_config(cfg='config/multi_ssap_config.yaml'):
    """
    Loads config from the yaml file
    """
    # cfg = 'config/multi_ssap_config.yaml'
    with open(cfg) as f:
        config = yaml.load(f)
    return config

def check_data():
    ssap = SSAP_Poisson(19, 14.135)
    actions = np.load('test_results/May-14-2021/17:27:51/results.npy')
    counts_data_loc = 'robot_paths/count_data/stage:19_dist:30.0_sample:50/alpha_counts.npy'
    overlaps_loc = 'robot_paths/count_data/stage:19_dist:30.0_sample:50/alpha_overlaps.npy'

    count_data = np.load(counts_data_loc)
    overlaps = np.load(overlaps_loc)
    print(overlaps)
    overlaps = [tuple(vals) for vals in overlaps]
    
    print(actions)
    print(count_data)

    for alg_action in actions:
        reward = calc_penalty_reward(count_data, overlaps, alg_action[0], 19, 2,3,ssap)
        print('Alg Reward: ', reward)
        for rob in alg_action[0]:
            print([i for i,val in enumerate(rob) if val==1])

    # for alg_action in actions:
    #     reward = (alg_action*count_data)
    #     reward = np.sum(reward[~np.isnan(reward)])
    #     print('Alg Reward: ', reward)
    #     for rob in alg_action[0]:
    #         print([i for i,val in enumerate(rob) if val==1])
    # best = 0

    # for rob_obs in count_data:
    #     rob_obs = rob_obs[~np.isnan(rob_obs)]
    #     print(rob_obs[rob_obs.argsort()[-3:]])
    #     best += np.sum(rob_obs[rob_obs.argsort()[-3:]])
    # print('Best: ', best) 


# ======== Multi Robot ========
def multi_oracle_reward(multi_obs, total_deploy):
    total_reward = 0
    for single_run_observations in multi_obs:
        oracle_reward = np.sum(single_run_observations[single_run_observations.argsort()][-total_deploy:])
        total_reward += oracle_reward
    return total_reward

def multi_mcts_run(obs, oracle_reward, total_ground_robots, total_deploy, total_stages, ssap, overlaps, mcts_params):
    """
    Takes in a single set of observations and returns the reward from MCTS approach

    """
    mcts_reward = []
    robot_actions = np.zeros((total_ground_robots, total_stages))
    num_actions = 2
    action_class = MultiRobotActions(num_actions, total_ground_robots)
    action_set = action_class.action_list
    # print('Num Actions: ', len(action_set))
    max_iterations = mcts_params['max_iterations']
    explore_param = mcts_params['explore_param']
    mcts_mode = mcts_params['mcts_mode']

    robot_deploy_budgets = np.ones(total_ground_robots) * total_deploy 
    total_robots = total_deploy * total_ground_robots

    # print(np.argwhere(np.isnan(obs)))

    ssap_stage_count = [np.count_nonzero(~np.isnan(rob_obs)) for rob_obs in obs]
    # ssap.robot_true_stage = [1 for i in range(total_ground_robots)]
    ssap.real_obs = obs
    curr_obs = np.zeros(total_ground_robots)

    for stage in tqdm.tqdm(range(total_stages)):
    # for stage in range(total_stages):
        ssap.current_stage = stage
        for i in range(total_ground_robots): 
            curr_obs[i] = obs[i][stage]
        stages_remaining = total_stages - stage
        # Is it right if we skip planning entirely if we have 0 deploys? Seems right
        past_obs = robot_actions*obs
        
        # send this into ssap class
        ssap_adaptive_idx = gen_overlap_idx(overlaps, robot_actions) 
        # runs each stage since the overlap_idx can change from robot actions
        ssap.adaptive_thresholds = ssap.gen_DRL_adaptive_thresh(n_stage=total_stages, overlap_idx=ssap_adaptive_idx, stage_counts=ssap_stage_count)
        
        # for rob_id in ssap.adaptive_thresholds:
        #     print(ssap.adaptive_thresholds[rob_id].shape)
        #     pprint.pprint(ssap.adaptive_thresholds[rob_id])
        
        if not np.all(robot_deploy_budgets == 0):
            [solution, root, list_of_all_nodes, winner] = multi_mcts_real_data(mcts_mode, action_set, stages_remaining, curr_obs, 
                                                                robot_deploy_budgets, max_iterations, explore_param, 
                                                                ssap, overlaps, past_obs, total_robots)
            best_action = solution[0]
            # deploy actions
            deploy_actions = best_action.deploy_actions
            for i, rob_deploy in enumerate(deploy_actions):
                # robot id i deploys
                if rob_deploy == 1:
                    robot_deploy_budgets[i] -= 1
                    robot_actions[i, stage] = 1

                    mcts_reward.append(curr_obs[i])
                    # may need something here to limit the deploy budget to be 0 and not negative
        
        # if stage == 1:
        #     sys.exit()
        
        # print(ssap.robot_true_stage)
        # ssap.robot_true_stage = [x+1 if not np.isnan(curr_obs[rob_id]) else x for rob_id,x in enumerate(ssap.robot_true_stage)]
    return robot_actions

def multi_ssap_run(obs, oracle_reward, total_ground_robots, total_deploy, total_stages, ssap, overlaps):
    thresholds = ssap.thresholds
    action_list = []
    for rob_id, rob_obs in enumerate(obs):
        current_stage = 1
        ssap_deploys = 0
        ssap_reward = []
        ssap_no_penalty = []
        rob_actions = []
        while ssap_deploys < total_deploy:
            deploys_remaining = total_deploy - ssap_deploys
            stages_remaining = total_stages - current_stage 
            
            current_obs = obs[rob_id][current_stage - 1]
            stage_threshold = np.trim_zeros(thresholds[:,-current_stage], trim='b')
            thresh_val = stage_threshold[-(deploys_remaining+1)]

            if (current_obs > thresh_val) or (deploys_remaining == stages_remaining):
                ssap_reward.append(current_obs)
                ssap_deploys += 1
                rob_actions.append(1)
            else:
                rob_actions.append(0)
            current_stage += 1
        pad_val = total_stages - len(rob_actions)
        rob_actions += [0] * (pad_val)
        action_list.append(rob_actions)
    # action_sequence = toActionMultiActionSequence(action_list)

    # total_budget = (total_stages, total_deploy)
    # # this reward comes out normalized. Need to un-normalize
    # ssap_reward = multi_ssap_reward(action_sequence, obs, total_budget, ssap, overlaps, None)
    # ssap_reward = ssap_reward * total_budget[1] * ssap.poisson_normalization_nintey * total_ground_robots

    # # calculate the unpenalized rewards
    # action_mask = np.ma.make_mask(action_list)
    # deployed_obs = obs*action_mask

    # print('SSAP Unpenalized: ', np.sum(deployed_obs))
    # print('SSAP Penalized: ', ssap_reward)
    # print(np.array(obs))
    # print(np.array(action_list))
    return np.array(action_list)

def calc_penalty_reward(obs, overlaps, action_list, total_stages, total_ground_robots, total_deploy, ssap):
    action_sequence = toActionMultiActionSequence(action_list)
    # this reward comes out normalized. Need to un-normalize
    total_budget = (total_stages, total_deploy)
    reward = multi_ssap_reward(action_sequence, obs, total_budget, ssap, overlaps, None, total_ground_robots*total_deploy)
    reward = reward * total_deploy * ssap.poisson_normalization_nintey * total_ground_robots

    return reward

def gen_overlap_idx(overlaps, robot_actions):

    """
    parameters
    ----------
    
    overlaps : [(o1, o2, ...)]
        list of tuples of overlaps. Each tuple has the form (r1, r2,...) where the r value represents the
        stage that the robot overlaps with another robot

    robot_actions: np.array((total_ground_robots, total_stages))
        Total ground robots x total stages

    
    returns
    -------
    overlap_idx : {robot_id : [(s,p)]}
        S = stage where robot has the overlap(s) and P = penalty, so number of overlaps
    
    """

    overlap_idx = {}

    # for each robot and their current actions, find which robot will have future overlaps 
    for rob_id, rob_actions in enumerate(robot_actions):
        rob_overlap_idx = {}

        # check each overlap, to see if the robot has an overlap stage ahead of other robots
        for overlap in overlaps:
            if overlap[rob_id] is not None:
                curr_rob_stage = overlap[rob_id]
            # robot has no overlap
            else:
                continue

            stage_penalty = (curr_rob_stage,0)
            
            # check each robot's overlap stage
            for overlap_rob_id, overlap_val in enumerate(overlap):
                # handles the same robot situation
                if rob_id == overlap_rob_id:
                    continue

                # the robot stage is ahead of (or at) other robot's overlap stage
                if overlap_val is not None and curr_rob_stage >= overlap_val:
                    # check if that robot's stage has a deploy action in robot actions
                    other_rob_action = robot_actions[overlap_rob_id][overlap_val]

                    # other robot deploys 
                    if other_rob_action == 1:
                        stage_penalty = (stage_penalty[0], stage_penalty[1]+1)
            
            if stage_penalty[1] is not 0:
                if stage_penalty[0] not in rob_overlap_idx:
                    rob_overlap_idx[stage_penalty[0]] = stage_penalty[1]
                else:
                    rob_overlap_idx[stage_penalty[0]] += stage_penalty[1]
                
        add_idx = [(key,rob_overlap_idx[key]) for key in rob_overlap_idx]
       
        overlap_idx[rob_id] = add_idx

    return overlap_idx


if __name__ == "__main__":
    start = time.time()
    real_data_multi_comparison_run()
    # check_data()
    end = time.time()
    print('Time: ', (end-start)/3600.0)

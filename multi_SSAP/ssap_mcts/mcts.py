"""
Chris Lee with help from Graeme Best's code

"""

from node import Node
from cost import cost, ssap_cost, ssap_multi_cost, ssap_multi_cost_overbudget
from rollout import random_rollout, ssap_rollout, random_multi_rollout, ssap_multi_rollout
from reward import simple_reward, ssap_reward, multi_ssap_reward
from action import tolistMultiActionSequence

import numpy as np
import copy
import random
import math
import tqdm
import matplotlib.pyplot as plt
import pickle

def mcts(mcts_mode, total_children, total_stages, curr_obs, deploy_budget, max_iters, explore_param, ssap):
    """
    mcts_mode : int
        type of mode for MCTS
        mode 0 = random rollouts
        mode 1 = ssap rollouts

    total_children : [ints]
        list of children available at each node
    
    total_stages : int
        total stages

    curr_obs : int
        current observation 

    deploy_budget : int
        number of drones per robot

    max_iters : int
        total iterations of MCTS
    
    explore_param : float
        UCB explore parameter

    ssap : class object
        poisson_mu : int
            mean of the poisson distribution
        overlap_paths : [(r1, r2, ... rg), ...]
            list of tuples, where each tuple contains the location (ID'd by stage number) that overlaps with another robot's path.
            For example, if r1 at location stage 5 overlaps r2 at location stage 7, the tuple would be (5,7)

    """

    #  ======================== Setup ========================
    # mcts stuff
    total_budget = (total_stages, deploy_budget)
    start_sequence = []
    feasible_children = gen_viable_children(start_sequence, copy.deepcopy(total_children), total_budget)
    root_node = Node(parent=None, sequence=start_sequence, stage_id = 0, budget=total_budget, 
                     unpicked_children=feasible_children, poisson_mu=ssap.lamb, total_stages=total_stages) 
    
    all_nodes = []
    all_nodes.append(root_node)

    # DEBUG
    ucb_list = []

    # ======================== Main loop ========================
    # for iter in tqdm.tqdm(range(max_iters)):
    for iter in range(max_iters):
        # print('Iterations: ', iter)
        
        #  ======================== Select and Expand  ========================
        current_node = root_node
        while True:
            unpicked_children = current_node.unpicked_children
            # print('Node: ', unpicked_children)
            # Find child node to expand, if not empty
            if unpicked_children: 
                # pick to expand a random child
                new_child = random.choice(unpicked_children)
                # remove child from the unpicked list
                unpicked_children.remove(new_child)

                # setup new sequence
                new_child_sequence = copy.deepcopy(current_node.sequence)
                new_child_sequence.append(new_child)
                new_child_budget = tuple(np.array(total_budget) - np.array(cost(new_child_sequence)))


                # setup new child's unpicked children
                new_child_unpicked_children = gen_viable_children(new_child_sequence, total_children, total_budget)

                # finally, set up the child node
                new_stage_id = current_node.stage_id + 1
                new_child_node = Node(parent=current_node, sequence=new_child_sequence, stage_id=new_stage_id, budget=new_child_budget, unpicked_children=new_child_unpicked_children)

                # update current node
                current_node.children.append(new_child_node)
                current_node = new_child_node
                all_nodes.append(new_child_node) # debugging

                # do not go deeper. Expanded here, then go to the rollout portion
                break


            # No more child nodes to expand, UCB down
            else:
                # At the end of the planning horizon
                if not current_node.children:
                    break
                # Select using UCB
                else:
                    # Define the UCB
                    def ucb(average, n_parent, n_child):
                        return average + explore_param * math.sqrt( (2*math.log(n_parent)) / float(n_child) )

                    # Pick children that maximizes the UCB
                    num_parent_sims = current_node.update_count
                    best_child = -1
                    best_ucb_score = 0
                    for child in current_node.children:
                        ucb_score = ucb(child.avg_score, num_parent_sims, child.update_count)
                        if best_child == -1 or (ucb_score > best_ucb_score):
                            best_child = child
                            best_ucb_score = ucb_score
                            # print('UCB: ', best_ucb_score)
                            # ucb_list.append(best_ucb_score)

                    # Recurse down the tree
                    current_node = best_child


        observation_vals = ssap._gen_poisson_obs_vals(total_stages)
        observation_vals[0] = curr_obs
        #  ======================== Rollout  ========================
        if mcts_mode == 0:
            rollout_sequence = random_rollout(subsequence=current_node.sequence, action_set=total_children, total_budget=total_budget)
            rollout_reward = ssap_reward(action_sequence=rollout_sequence, obs_vals=observation_vals, budget=total_budget, ssap=ssap)

        elif mcts_mode == 1:
            rollout_sequence = ssap_rollout(subsequence=current_node.sequence, action_set=total_children, obs_vals=observation_vals, total_budget=total_budget, ssap=ssap)
            rollout_reward = ssap_reward(action_sequence=rollout_sequence, obs_vals=observation_vals, budget=total_budget, ssap=ssap)

        #  ======================== Backprop  ========================
        # Update the stats of all the nodes up from current to root node
        parent = current_node
        while parent:
            parent.updateAverage(rollout_reward)
            parent = parent.parent


    # ======================== Solution ========================
    current = root_node
    while current_node.children:
        best_score = 0
        best_child = -1
        for child in current_node.children:
            score = child.avg_score
            if best_child == -1 or (score>best_score):
                best_child = child
                best_score = score
        
        current = best_child

    solution = current_node.sequence
    winner = current_node

    # plt.plot(ucb_list)
    # plt.show()

    return [solution, root_node, all_nodes, winner]

def gen_viable_children(curr_seq, total_children, budget):
    # adds the valid children only if that selected children will not exceed the budget
    # in our case, we don't add children that will deploy if we are already at the deploy limit
    def is_overbudget(a):
        seq_copy = copy.deepcopy(curr_seq)
        seq_copy.append(a)

        new_seq_cost = np.array(ssap_cost(seq_copy)) 
        diff = new_seq_cost - np.array(budget)
        return np.any(diff > 0)


    viable_children = [child for child in total_children if not is_overbudget(child)]

    return viable_children

def multi_mcts(mcts_mode, total_children, total_stages, curr_obs, deploy_budget, max_iters, 
               explore_param, ssap, overlaps, past_obs, total_robots):
    """
    mcts_mode : int
        type of mode for MCTS
        mode 0 = random rollouts
        mode 1 = ssap rollouts

    total_children : [ints]
        list of children available at each node
    
    total_stages : int
        total stages

    curr_obs : np.array(ints)
        current observation 

    deploy_budget : np.array(int)
        number of drones per each robot

    max_iters : int
        total iterations of MCTS
    
    explore_param : float
        UCB explore parameter

    ssap : class object
        poisson_mu : int
            mean of the poisson distribution
        overlap_paths : [(r1, r2, ... rg), ...]
            list of tuples, where each tuple contains the location (ID'd by stage number) that overlaps with another robot's path.
            For example, if r1 at location stage 5 overlaps r2 at location stage 7, the tuple would be (5,7)

    overlaps : [o1, o2, ...]
        list of tuples of overlaps. Each tuple has the form (r1, r2,...) where the r value represents the
        stage that the robot overlaps with another robot 

    past_obs : np.array((num_robs, total_stages))
        previously captured observations

    """

    #  ======================== Setup ========================
    # mcts stuff
    total_budget = (total_stages, deploy_budget)
    # print('Total Budget: ', total_budget)
    start_sequence = []
    # filter out infeasible unpicked childrens
    feasible_children = gen_multi_viable_children(start_sequence, copy.deepcopy(total_children), total_budget)
    # feasible_children = copy.deepcopy(total_children)
    # print('Num Children: ', len(feasible_children))
    root_node = Node(parent=None, sequence=start_sequence, stage_id = 0, budget=total_budget, 
                     unpicked_children=copy.deepcopy(feasible_children), poisson_mu=ssap.lamb, total_stages=total_stages) 
    all_nodes = []
    all_nodes.append(root_node)

    # DEBUG
    ucb_list = []

    # ======================== Main loop ========================
    # for iter in tqdm.tqdm(range(max_iters)):
    for iter in range(max_iters):
        # print('Iterations: ', iter)
        
        #  ======================== Select and Expand  ========================
        current_node = root_node
        while True:
            unpicked_children = current_node.unpicked_children
            # print('Node: ', unpicked_children)
            # Find child node to expand, if not empty
            if unpicked_children: 
                # pick to expand a random child
                new_child = random.choice(unpicked_children)
                # remove child from the unpicked list
                unpicked_children.remove(new_child)

                # setup new sequence
                new_child_sequence = copy.deepcopy(current_node.sequence)
                new_child_sequence.append(new_child)

                new_child_cost = ssap_multi_cost(new_child_sequence)
                stages_remaining = total_budget[0] - new_child_cost[0][0]
                remaining_deploys = []
                for rob in new_child_cost:
                    remaining_deploys.append(rob[1])
                new_child_budget = tuple((stages_remaining, np.array(remaining_deploys)))
                

                # setup new child's unpicked children
                new_child_unpicked_children = gen_multi_viable_children(new_child_sequence, copy.deepcopy(feasible_children), total_budget)
                
                # finally, set up the child node
                new_stage_id = current_node.stage_id + 1
                new_child_node = Node(parent=current_node, sequence=new_child_sequence, stage_id=new_stage_id, 
                                      budget=new_child_budget, unpicked_children=new_child_unpicked_children)

                # update current node
                current_node.children.append(new_child_node)
                current_node = new_child_node
                all_nodes.append(new_child_node) # debugging

                # do not go deeper. Expanded here, then go to the rollout portion
                break


            # No more child nodes to expand, UCB down
            else:
                # At the end of the planning horizon
                if not current_node.children:
                    break
                # Select using UCB
                else:
                    # Define the UCB
                    def ucb(average, n_parent, n_child):
                        return average + explore_param * math.sqrt( (2*math.log(n_parent)) / float(n_child) )

                    # Pick children that maximizes the UCB
                    num_parent_sims = current_node.update_count
                    best_child = -1
                    best_ucb_score = 0
                    for child in current_node.children:
                        ucb_score = ucb(child.avg_score, num_parent_sims, child.update_count)
                        if best_child == -1 or (ucb_score > best_ucb_score):
                            best_child = child
                            best_ucb_score = ucb_score
                            # print('UCB: ', best_ucb_score)
                            # ucb_list.append(best_ucb_score)

                    # Recurse down the tree
                    current_node = best_child

        ground_rob_num = len(curr_obs)
        observation_vals = ssap._gen_multi_poisson_obs_vals(ground_rob_num, total_stages)
        # print(curr_obs)
        for i, obs in enumerate(curr_obs):
            observation_vals[i][0] = obs
        #  ======================== Rollout  ========================
        if mcts_mode == 0:
            rollout_sequence = random_multi_rollout(subsequence=current_node.sequence, action_set=total_children, 
                                                    total_budget=total_budget)
            rollout_reward = multi_ssap_reward(action_sequence=rollout_sequence, obs_vals=observation_vals, 
                                               budget=total_budget, ssap=ssap, overlaps=overlaps, 
                                               past_obs=past_obs, total_robots=total_robots)

        elif mcts_mode == 1:
            rollout_sequence = ssap_multi_rollout(subsequence=current_node.sequence, action_set=total_children, 
                                                  obs_vals=observation_vals, total_budget=total_budget, ssap=ssap)
            rollout_reward = multi_ssap_reward(action_sequence=rollout_sequence, obs_vals=observation_vals, 
                                               budget=total_budget, ssap=ssap, overlaps=overlaps, 
                                               past_obs=past_obs, total_robots=total_robots)

        #  ======================== Backprop  ========================
        # Update the stats of all the nodes up from current to root node
        parent = current_node
        while parent:
            parent.updateAverage(rollout_reward)
            parent = parent.parent


    # ======================== Solution ========================
    current = root_node
    while current_node.children:
        best_score = 0
        best_child = -1
        for child in current_node.children:
            score = child.avg_score
            if best_child == -1 or (score>best_score):
                best_child = child
                best_score = score
        
        current = best_child

    solution = current_node.sequence
    winner = current_node

    # plt.plot(ucb_list)
    # plt.show()

    return [solution, root_node, all_nodes, winner]

def gen_multi_viable_children(curr_seq, total_children, budget):
    # adds the valid children only if that selected children will not exceed the budget
    # in our case, we don't add children that will deploy if we are already at the deploy limit
    # print('Sequence In Gen:', np.array(tolistMultiActionSequence(curr_seq)))
    # print('Gen Input Total Children: ',len(total_children))
    # print('Budget: ', budget)
    def is_overbudget(a):
        seq_copy = copy.deepcopy(curr_seq)
        seq_copy.append(a)

        return ssap_multi_cost_overbudget(seq_copy, budget)


    viable_children = [child for child in total_children if not is_overbudget(child)]
    # print('Budget: ', budget)
    # print(len(viable_children))
    return viable_children

def multi_mcts_real_data(mcts_mode, total_children, total_stages, curr_obs, deploy_budget, max_iters, 
               explore_param, ssap, overlaps, past_obs, total_robots):
    """
    mcts_mode : int
        type of mode for MCTS
        mode 0 = random rollouts
        mode 1 = ssap rollouts

    total_children : [ints]
        list of children available at each node
    
    total_stages : int
        total stages

    curr_obs : np.array(ints)
        current observation 

    deploy_budget : np.array(int)
        number of drones per each robot

    max_iters : int
        total iterations of MCTS
    
    explore_param : float
        UCB explore parameter

    ssap : class object
        poisson_mu : int
            mean of the poisson distribution
        overlap_paths : [(r1, r2, ... rg), ...]
            list of tuples, where each tuple contains the location (ID'd by stage number) that overlaps with another robot's path.
            For example, if r1 at location stage 5 overlaps r2 at location stage 7, the tuple would be (5,7)

    overlaps : [o1, o2, ...]
        list of tuples of overlaps. Each tuple has the form (r1, r2,...) where the r value represents the
        stage that the robot overlaps with another robot 

    past_obs : np.array((num_robs, total_stages))
        previously captured observations

    """

    #  ======================== Setup ========================
    # mcts stuff
    total_budget = (total_stages, deploy_budget)
    # print('Total Budget: ', total_budget)
    start_sequence = []
    # filter out infeasible unpicked childrens
    feasible_children = gen_multi_viable_children_real_data(start_sequence, copy.deepcopy(total_children), total_budget, curr_obs)
    # feasible_children = copy.deepcopy(total_children)
    print('Num Children: ', len(feasible_children))
    for child in feasible_children:
        print(child.deploy_actions)
    root_node = Node(parent=None, sequence=start_sequence, stage_id = 0, budget=total_budget, 
                     unpicked_children=copy.deepcopy(feasible_children), poisson_mu=ssap.lamb, total_stages=total_stages) 
    all_nodes = []
    all_nodes.append(root_node)

    # DEBUG
    ucb_list = []

    # ======================== Main loop ========================
    # for iter in tqdm.tqdm(range(max_iters)):
    for iter in range(max_iters):
        # print('Iterations: ', iter)
        
        #  ======================== Select and Expand  ========================
        current_node = root_node
        while True:
            unpicked_children = current_node.unpicked_children
            # print('Node: ', unpicked_children)
            # Find child node to expand, if not empty
            if unpicked_children: 
                # pick to expand a random child
                new_child = random.choice(unpicked_children)
                # remove child from the unpicked list
                unpicked_children.remove(new_child)

                # setup new sequence
                new_child_sequence = copy.deepcopy(current_node.sequence)
                new_child_sequence.append(new_child)

                new_child_cost = ssap_multi_cost(new_child_sequence)
                stages_remaining = total_budget[0] - new_child_cost[0][0]
                remaining_deploys = []
                for rob in new_child_cost:
                    remaining_deploys.append(rob[1])
                new_child_budget = tuple((stages_remaining, np.array(remaining_deploys)))
                

                # setup new child's unpicked children
                new_child_unpicked_children = gen_multi_viable_children_real_data(new_child_sequence, copy.deepcopy(feasible_children), total_budget, curr_obs)
                
                # finally, set up the child node
                new_stage_id = current_node.stage_id + 1
                new_child_node = Node(parent=current_node, sequence=new_child_sequence, stage_id=new_stage_id, 
                                      budget=new_child_budget, unpicked_children=new_child_unpicked_children)

                # update current node
                current_node.children.append(new_child_node)
                current_node = new_child_node
                all_nodes.append(new_child_node) # debugging

                # do not go deeper. Expanded here, then go to the rollout portion
                break


            # No more child nodes to expand, UCB down
            else:
                # At the end of the planning horizon
                if not current_node.children:
                    break
                # Select using UCB
                else:
                    # Define the UCB
                    def ucb(average, n_parent, n_child):
                        return average + explore_param * math.sqrt( (2*math.log(n_parent)) / float(n_child) )

                    # Pick children that maximizes the UCB
                    num_parent_sims = current_node.update_count
                    best_child = -1
                    best_ucb_score = 0
                    for child in current_node.children:
                        ucb_score = ucb(child.avg_score, num_parent_sims, child.update_count)
                        if best_child == -1 or (ucb_score > best_ucb_score):
                            best_child = child
                            best_ucb_score = ucb_score
                            # print('UCB: ', best_ucb_score)
                            # ucb_list.append(best_ucb_score)

                    # Recurse down the tree
                    current_node = best_child

        ground_rob_num = len(curr_obs)
        observation_vals = ssap._gen_multi_poisson_obs_vals(ground_rob_num, total_stages)
        
        for i, obs in enumerate(curr_obs):
            if np.isnan(obs):
                observation_vals[i][0] = -9000
            else:
                observation_vals[i][0] = obs

        # add -9000 to the observation stages, but relate it to the current observation stages
        for rob_id, rob_obs in enumerate(observation_vals):
            true_nan_idx = [int(val)-ssap.current_stage for val in np.argwhere(np.isnan(ssap.real_obs[rob_id]))]
            pseudo_nan_idx = [val for val in true_nan_idx if val>=0]

            for idx in pseudo_nan_idx:
                observation_vals[rob_id][idx] = -9000

        #  ======================== Rollout  ========================
        if mcts_mode == 0:
            rollout_sequence = random_multi_rollout(subsequence=current_node.sequence, action_set=total_children, 
                                                    total_budget=total_budget)
            rollout_reward = multi_ssap_reward(action_sequence=rollout_sequence, obs_vals=observation_vals, 
                                               budget=total_budget, ssap=ssap, overlaps=overlaps, 
                                               past_obs=past_obs, total_robots=total_robots)

        elif mcts_mode == 1:
            rollout_sequence = ssap_multi_rollout(subsequence=current_node.sequence, action_set=total_children, 
                                                  obs_vals=observation_vals, total_budget=total_budget, ssap=ssap)
            rollout_reward = multi_ssap_reward(action_sequence=rollout_sequence, obs_vals=observation_vals, 
                                               budget=total_budget, ssap=ssap, overlaps=overlaps, 
                                               past_obs=past_obs, total_robots=total_robots)

        #  ======================== Backprop  ========================
        # Update the stats of all the nodes up from current to root node
        parent = current_node
        while parent:
            parent.updateAverage(rollout_reward)
            parent = parent.parent


    # ======================== Solution ========================
    current = root_node
    while current_node.children:
        best_score = 0
        best_child = -1
        for child in current_node.children:
            score = child.avg_score
            if best_child == -1 or (score>best_score):
                best_child = child
                best_score = score
        
        current = best_child

    solution = current_node.sequence
    winner = current_node

    # plt.plot(ucb_list)
    # plt.show()

    return [solution, root_node, all_nodes, winner]

def gen_multi_viable_children_real_data(curr_seq, total_children, budget, curr_obs):
    # adds the valid children only if that selected children will not exceed the budget
    # in our case, we don't add children that will deploy if we are already at the deploy limit
    # print('Sequence In Gen:', np.array(tolistMultiActionSequence(curr_seq)))
    # print('Gen Input Total Children: ',len(total_children))
    # print('Budget: ', budget)
    def is_overbudget(a):
        seq_copy = copy.deepcopy(curr_seq)
        seq_copy.append(a)

        return ssap_multi_cost_overbudget(seq_copy, budget)

    def current_rob_deploy_action(child, rob_id):
        """
        Input is a child, which is an Action class
        """
        # should be a tuple of form (a1, a2, a3...)
        deploy_actions = child.deploy_actions
        if deploy_actions[rob_id] == 1:
            return True
        else:
            return False

    #  clear out based on budget
    viable_children = [child for child in total_children if not is_overbudget(child)]
    
    # clear out based on None deploy observations 
    for rob_id, obs_vals in enumerate(curr_obs):
        # remove rob_id deploy child
        if np.isnan(obs_vals):
            viable_children = [child for child in viable_children if not current_rob_deploy_action(child,rob_id)]

    return viable_children


import numpy as np
import itertools
import time
import sys

def gen_action_list_from_obs_idx(obs_idx, ground_robots):
    """
    obs_idx : tuple ((o1, o2,...), (o1, o2,...), ....)
    """

    total_actions = []

    for obs in obs_idx:
        rob_action = []
        for i in range(num_stages):
            if i in obs:
                rob_action.append(1)
            else:
                rob_action.append(0)
        total_actions.append(rob_action)
    return total_actions


def calc_penalty_reward(obs, overlaps, action_list, total_stages, total_ground_robots, total_deploy, ssap):
    action_sequence = toActionMultiActionSequence(action_list)
    # this reward comes out normalized. Need to un-normalize
    total_budget = (total_stages, total_deploy)
    reward = multi_ssap_reward(action_sequence, obs, total_budget, ssap, overlaps, None, total_ground_robots*total_deploy)
    reward = reward * total_deploy * total_ground_robots

    return reward                
                
def multi_robot_reward(robot_actions, obs_vals, num_carrier_robots, overlaps):
    # return a list of lists of all the robot actions
    robot_actions = np.stack(robot_actions, axis=0)
    # print(robot_actions)
    # print(robot_actions.shape)
    # print(obs_vals)
    # print(obs_vals.shape)
    # obs_vals = np.stack(obs_vals, axis=0)

    action_mask = np.ma.make_mask(robot_actions)
    deployed_obs = obs_vals*action_mask

    penalized_deployed_obs = calc_overlap_norm_penalty(deployed_obs, num_carrier_robots, overlaps)

    reward = np.sum(penalized_deployed_obs)

    return reward

def calc_overlap_norm_penalty(deployed_obs, num_carrier_robots, overlaps):
    # higher penalty term really hurts the overlaps
    penalty_term = 1

    ground_rob = num_carrier_robots
    stages = len(deployed_obs[0])
    penalty = np.ones((ground_rob, stages))
    if not overlaps:
        return deployed_obs

    for overlap in overlaps:
        overlap_mask = np.zeros((ground_rob, stages))
        for rob_id, rob in enumerate(overlap):
            if rob is not None:
                overlap_mask[rob_id,rob] = 1
        overlap_mask = np.ma.make_mask(overlap_mask)
        overlap_deployed_obs = deployed_obs*overlap_mask
        total_overlaps = np.count_nonzero(overlap_deployed_obs)
        
        # If at least two robots overlap
        if total_overlaps > 1:
            penalty += (overlap_mask*penalty_term)
    
    return (deployed_obs/penalty)


if __name__ == "__main__":
    start = time.time()
    test = '/home/chris/grad_hw/marsupial_robotics/multi_SSAP/ssap_mcts/test_results/ssap_improved_rollout_25k/e0.2/ssap_r3_25k_o2_d3_s10_e0.2/99/obs.npy'

    overlaps = [(1,5,None),(3,2,5),
                (None, 5,7),(8,5,9),
                (4, None, 1),(None, 3,7),
                (3,6,4),(9,8,3),
                (5,3,5),(2,7,1)]

    deploys = 3
    ground_robots = 3
    num_stages = 10

    obs = np.load(test)
    best = 0
    for rob_obs in obs:
        print(rob_obs[rob_obs.argsort()[-deploys:]])
        best += np.sum(rob_obs[rob_obs.argsort()[-deploys:]])
    print(best) 
    

    # # single robot, combination of all possible indexes of observations
    # obs_comb = list(itertools.combinations(range(num_stages),deploys))
    # obs_total = []
    # for i in range(ground_robots):
    #     obs_total.append(obs_comb)

    # # multi robot, product of all observations from all robots 
    # obs_perm = list(itertools.product(*obs_total))
    # all_actions = [gen_action_list_from_obs_idx(obs,ground_robots) for obs_val in obs_perm]

    # print('Observation Count: ', len(obs_perm))
    # print('Action Space: ', len(all_actions))

    # np.save('total_action_space.npy',np.array(all_actions))

    # sys.exit()

    # best_reward = 0.0

    # for action in all_actions:
    #     reward = multi_robot_reward(action, obs, ground_robots, overlaps)
    #     if reward > best_reward:
    #         best_reward = reward
    
    # end = time.time()
    # print('Time: ', (end-start)/3600.0)
    # print(best_reward)

    max_idx = []
    for rob_obs in obs:
        max_idx.append(list(rob_obs.argsort()[-deploys:][::-1]))

    print('Initial Max Idx: ', max_idx)

    for rob_id, rob_obs in enumerate(obs):
        rob_idx = max_idx[rob_id]
        # the max_idx of other robots
        max_idx_without_rob = [x for i,x in enumerate(max_idx) if i!=rob_id]
        
        # find if best observation is in an overlap spot
        for obs_idx in rob_idx:
            for overlap in overlaps:
                # obs_idx of rob_id is in that overlap tuple
                if obs_idx == overlap[rob_id]:
                    print('Robot: ', rob_id)
                    print('Obs Idx: ', obs_idx)
                    print('Overlaps: ', overlap)
                    
                    # now we find if this obs_idx is also in our max_idx, check other robots in this overlap tuple
                    for overlap_rob_id, rob_overlap_stage in enumerate(overlap):
                        if rob_id == overlap_rob_id:
                            continue
                        else:
                            for other_rob_idx in max_idx_without_rob:
                                # the robot stages in other overlaps are in this max_idx as well
                                if rob_overlap_stage == other_rob_idx[overlap_rob_id]:
                                    print('Rob Overlap Stage: ', rob_overlap_stage)
                                    print('Offending Max Idx: ',other_rob_idx)
                                    print('Offending Robot Id: ', overlap_rob_id)
                                    # there is an offender here so either switch this robot or other robot's id
                                    
                                    # check rob_id

                                    # check overlap_rob_id
                    print('')    
import numpy as np



counts_data_loc = '/home/chris/grad_hw/marsupial_robotics/multi_SSAP/ssap_mcts/robot_paths/count_data/stage:19_dist:5.0_sample:50/alpha_counts.npy'
overlaps_loc = '/home/chris/grad_hw/marsupial_robotics/multi_SSAP/ssap_mcts/robot_paths/count_data/stage:19_dist:5.0_sample:50/alpha_overlaps.npy'

count_data = np.load(counts_data_loc)
overlaps = np.load(overlaps_loc)

total_deploy = 3

print(count_data)
print(overlaps)

for rob_id, rob_obs in enumerate(count_data):
    rob_obs = rob_obs[~np.isnan(rob_obs)]
    oracle_reward = np.sum(rob_obs[rob_obs.argsort()][-total_deploy:])
    print(oracle_reward)
import numpy as np
import scipy.stats as stats
import yaml
import pprint

import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

import ssap


def gen_overlap_idx(overlaps, robot_actions):

    """
    parameters
    ----------
    
    overlaps : [(o1, o2, ...)]
        list of tuples of overlaps. Each tuple has the form (r1, r2,...) where the r value represents the
        stage that the robot overlaps with another robot

    robot_actions: np.array((total_ground_robots, total_stages))
        Total ground robots x total stages

    
    returns
    -------
    overlap_idx : {robot_id : [(s,p)]}
        S = stage where robot has the overlap(s) and P = penalty, so number of overlaps
    
    """

    overlap_idx = {}

    # for each robot and their current actions, find which robot will have future overlaps 
    for rob_id, rob_actions in enumerate(robot_actions):
        rob_overlap_idx = {}

        # check each overlap, to see if the robot has an overlap stage ahead of other robots
        for overlap in overlaps:
            curr_rob_stage = overlap[rob_id]
            stage_penalty = (curr_rob_stage,0)
            
            # check each robot's overlap stage
            for overlap_rob_id, overlap_val in enumerate(overlap):
                # handles the same robot situation
                if rob_id == overlap_rob_id:
                    continue

                # the robot stage is ahead of (or at) other robot's overlap stage
                if curr_rob_stage >= overlap_val and overlap_val is not None:
                    # check if that robot's stage has a deploy action in robot actions
                    other_rob_action = robot_actions[overlap_rob_id][overlap_val]

                    # other robot deploys 
                    if other_rob_action == 1:
                        stage_penalty = (stage_penalty[0], stage_penalty[1]+1)
            
            if stage_penalty[1] is not 0:
                if stage_penalty[0] not in rob_overlap_idx:
                    rob_overlap_idx[stage_penalty[0]] = stage_penalty[1]
                else:
                    rob_overlap_idx[stage_penalty[0]] += stage_penalty[1]
                
        add_idx = [(key,rob_overlap_idx[key]) for key in rob_overlap_idx]
       
        overlap_idx[rob_id] = add_idx

    return overlap_idx

def load_config():
    """
    Loads config from the yaml file
    """
    cfg = '/home/chris/grad_hw/marsupial_robotics/multi_SSAP/ssap_mcts/config/test.yaml'
    with open(cfg) as f:
        config = yaml.load(f)
    return config

config = load_config()
rob_actions = np.array([[0,1,0,0,0,1,0,0,1,0],[0,0,0,0,1,0,1,0,1,0],[0,1,0,1,0,0,0,1,0,0]])
# rob_actions = np.array([[0,1,0,1,0,1,0,0,1,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0]])
# rob_actions = np.zeros((3,10))


overlaps = config['test_params']['overlaps']

overlap_idx = gen_overlap_idx(overlaps, rob_actions)
# print('Actions')
# pprint.pprint(rob_actions)
# print('Overlaps')
# pprint.pprint(overlaps)
print('Overlap Idx')
pprint.pprint(overlap_idx)

ssap = ssap.SSAP_Poisson(10, 5)
robot_thresholds = ssap.gen_DRL_adaptive_thresh(10, overlap_idx)

print('Robot Thresholds')

for rob_id in robot_thresholds:
    print(robot_thresholds[rob_id])

# print(rob_actions)
# print(config['test_params']['overlaps'])
import numpy as np
import glob
import os 
import matplotlib.pyplot as plt
import collections
import scipy.stats as stats



# for result in glob.glob(folder_path + '/*.npy'):
#     # print(result)
#     data = np.load(result)
#     overlap = int(os.path.basename(result).split('.')[0])

#     oracle = data[:,0]
#     ssap = data[:,1]
#     mcts = data[:,2]

#     ssap_ratio[overlap] = ssap/oracle
#     mcts_ratio[overlap] = mcts/oracle

# ssap_ordered = collections.OrderedDict(sorted(ssap_ratio.items()))
# mcts_ordered = collections.OrderedDict(sorted(mcts_ratio.items()))

# data_len = len(ssap_ratio[0])
# ssap_x = [key for key in ssap_ordered]
# ssap_y = [np.average(ssap_ordered[key]) for key in ssap_ordered]
# ssap_y_err = [np.std(ssap_ordered[key])/np.sqrt(data_len) for key in ssap_ordered]

# mcts_x = [key for key in mcts_ordered]
# mcts_y = [np.average(mcts_ordered[key]) for key in mcts_ordered]
# mcts_y_err = [np.std(mcts_ordered[key])/np.sqrt(data_len) for key in mcts_ordered]

def gen_poisson_obs_vals(nums):
    lamb = 5
    obs_vals = stats.poisson.rvs(lamb, 0, nums)
    return obs_vals

def process_data(folder_path):
    """
    Assumes just two sets of data, SSAP and MCTS
    """
    ssap_ratio = {}
    mcts_ratio = {}
    random_ratio = {}
    processed_data = {}

    ground_robots = 3
    deploys = 3
    total_robots = ground_robots*deploys

    for result in glob.glob(folder_path + '/*.npy'):
        data = np.load(result)
        overlap = int(os.path.basename(result).split('.')[0])

        oracle = data[:,0]
        ssap = data[:,1]
        mcts = data[:,2]

        ssap_ratio[overlap] = ssap
        mcts_ratio[overlap] = mcts
        random_ratio[overlap] = np.sum(gen_poisson_obs_vals(total_robots))

    ssap_ordered = collections.OrderedDict(sorted(ssap_ratio.items()))
    mcts_ordered = collections.OrderedDict(sorted(mcts_ratio.items()))
    random_ordered = collections.OrderedDict(sorted(random_ratio.items()))

    data_len = len(ssap_ratio[2])

    ssap_x = [key for key in ssap_ordered]
    ssap_y = [np.average(ssap_ordered[key]) for key in ssap_ordered]
    ssap_y_err = [np.std(ssap_ordered[key])/np.sqrt(data_len) for key in ssap_ordered]
    processed_data['ssap_x'] = ssap_x
    processed_data['ssap_y'] = ssap_y
    processed_data['ssap_y_err'] = ssap_y_err

    mcts_x = [key for key in mcts_ordered]
    mcts_y = [np.average(mcts_ordered[key]) for key in mcts_ordered]
    mcts_y_err = [np.std(mcts_ordered[key])/np.sqrt(data_len) for key in mcts_ordered]
    processed_data['mcts_x'] = mcts_x
    processed_data['mcts_y'] = mcts_y
    processed_data['mcts_y_err'] = mcts_y_err

    random_x = [key for key in random_ordered]
    random_y = [np.average(random_ordered[key]) for key in random_ordered]
    random_y_err = [np.std(random_ordered[key])/np.sqrt(data_len) for key in random_ordered]
    processed_data['random_x'] = random_x
    processed_data['random_y'] = random_y
    processed_data['random_y_err'] = random_y_err

    return processed_data

path_random_rollout = '/home/chris/grad_hw/marsupial_robotics/multi_SSAP/ssap_mcts/test_results/rand_rollout_25k/e0.2/results/r3_25k_d3_s10'
path_ssap_rollout = '/home/chris/grad_hw/marsupial_robotics/multi_SSAP/ssap_mcts/test_results/ssap_improved_rollout_25k/e0.2/results/r3_o2_d3_s10'

ssap_ratio = {}
mcts_ratio = {}

rand_rollout_data = process_data(path_random_rollout)
ssap_rollout_data = process_data(path_ssap_rollout)

# plotting SSAP rollout
plt.errorbar(ssap_rollout_data['mcts_x'], ssap_rollout_data['mcts_y'], yerr=ssap_rollout_data['mcts_y_err'], 
             linestyle='--', marker='o', color='black', label='MCTS_SSAP_Rollout')
# plotting random rollouts
plt.errorbar(rand_rollout_data['mcts_x'], rand_rollout_data['mcts_y'], yerr=rand_rollout_data['mcts_y_err'], 
             linestyle='--', marker='o', color='red', label='MCTS_Random_Rollout')
# plotting SSAP 
plt.errorbar(rand_rollout_data['ssap_x'], rand_rollout_data['ssap_y'], yerr=rand_rollout_data['ssap_y_err'], 
             linestyle='-', marker='o', color='blue', label='Single Robot SSAP')
# plotting random
plt.errorbar(ssap_rollout_data['random_x'], ssap_rollout_data['random_y'], yerr=ssap_rollout_data['random_y_err'], 
             linestyle='-', marker='o', color='green', label='Random')


# plt.errorbar(ssap_rollout_data['ssap_x'], ssap_rollout_data['ssap_y'], yerr=ssap_rollout_data['ssap_y_err'], 
#              linestyle='-', marker='o', color='green', label='SSAP_SSAP_Rollout')

plt.xlabel('Overlaps', fontsize=14)
plt.xticks(fontsize=12)
plt.ylabel('Total System Reward', fontsize=14)
plt.yticks(fontsize=12)
# plt.title('Performance with SEM')
plt.legend(fontsize=12, loc='lower left')
plt.savefig('performance_comparison.eps',format='eps')
plt.show()


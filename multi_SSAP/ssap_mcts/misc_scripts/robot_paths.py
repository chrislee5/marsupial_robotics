import glob
import csv
import os
import datetime
import matplotlib.pyplot as plt
import numpy as np

# path = '/home/chris/grad_hw/marsupial_robotics/multi_SSAP/multi_robot_ws/robot_paths/grouped_paths/beta/*.txt'
path = '/home/chris/grad_hw/marsupial_robotics/multi_SSAP/multi_robot_ws/robot_paths/time_based/1x/*.txt'

data = {}

for rob in glob.glob(path):
    name = os.path.basename(rob).split('.')[0]
    data[name] = {}
    with open(rob) as csv_file:
        stage = []
        count = []
        rob_x = []
        rob_y = []
        rob_z = []
        time = []

        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            vals = row[0].split(':')
            stage.append(int(vals[0]))
            count.append(int(vals[1]))
            rob_x.append(float(vals[2]))
            rob_y.append(float(vals[3]))
            rob_z.append(float(vals[4]))
            time.append(float(vals[5]))
        
        data[name]['stage'] = stage
        data[name]['count'] = count
        data[name]['rob_x'] = rob_x
        data[name]['rob_y'] = rob_y
        data[name]['rob_z'] = rob_z
        data[name]['time'] = time

        start_time = time[0]
        diff_time = [j-i for i,j in zip(time[:-1],time[1:])]
        print('Average Diff: ', np.average(diff_time))
        print('STD: ', np.std(diff_time))
        time = [(time-start_time) for time in time]

        data[name]['diff_time'] = time

for rob in data:
    stage = data[rob]['stage']
    x_pos = [-val for val in data[rob]['rob_x']]
    y_pos = data[rob]['rob_y']
    plt.scatter(y_pos, x_pos, label=rob)

    for i,(x,y) in enumerate(zip(y_pos, x_pos)):
        plt.annotate(stage[i],(x,y))
ax = plt.gca()
ax.set_aspect(1.0)
plt.show()

# for rob in data:
#     stage = data[rob]['stage']
#     time = data[rob]['time']
#     plt.plot(stage, time, label=rob)
# plt.show()
diff_x = []
diff_y = []

min_time = 9999999999
for rob in data:
    time = data[rob]['time']
    x_pos = data[rob]['rob_x']
    y_pos = data[rob]['rob_y']
    start_time = time[0]

    if start_time < min_time:
        min_time = start_time
    
    x_start = x_pos[0]
    y_start = y_pos[0]

    diff_x.append(x_start)
    diff_y.append(y_start)

    dt_object = datetime.datetime.fromtimestamp(start_time)
    print(rob)
    print(dt_object)

x_diff = diff_x[1] - diff_x[0]
y_diff = diff_y[1] - diff_y[0]

for rob in data:
    data[rob]['shared_time'] = [round(time-min_time) for time in data[rob]['time']]
    print(rob)
    print(data[rob]['shared_time'])


print('Distance: ', np.sqrt((x_diff**2 + y_diff**2)))
import glob
import csv
import os
import datetime
import matplotlib.pyplot as plt
import numpy as np
import itertools
import pprint


def load_data(path):
    data = {}
    for rob in glob.glob(path):
        name = os.path.basename(rob).split('.')[0]
        data[name] = {}
        with open(rob) as csv_file:
            stage = []
            count = []
            rob_x = []
            rob_y = []
            rob_z = []
            time = []

            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                vals = row[0].split(':')
                stage.append(int(vals[0]))
                count.append(int(vals[1]))
                rob_x.append(float(vals[2]))
                rob_y.append(float(vals[3]))
                rob_z.append(float(vals[4]))
                time.append(float(vals[5]))
            
            data[name]['stage'] = stage
            data[name]['count'] = count
            data[name]['rob_x'] = rob_x
            data[name]['rob_y'] = rob_y
            data[name]['rob_z'] = rob_z
            data[name]['time'] = time

            start_time = time[0]
            diff_time = [j-i for i,j in zip(time[:-1],time[1:])]
            print('Average Diff: ', np.average(diff_time))
            print('STD: ', np.std(diff_time))
            time = [(time-start_time) for time in time]

            data[name]['diff_time'] = time
    return data

def load_processed_data(path):
    data = {}
    for rob in glob.glob(path):
        name = os.path.basename(rob).split('.')[0]
        data[name] = {}
        with open(rob) as csv_file:
            diff_time = []
            stage = []
            count = []
            rob_x = []
            rob_y = []
            rob_z = []

            csv_reader = csv.reader(csv_file, delimiter=',')
            for i, row in enumerate(csv_reader):
                if i == 0:
                    continue
                #  this is gonna be gross
                if row[0] == 'NA':
                    diff_time.append(None)
                else:
                    diff_time.append(float(row[0]))

                if row[1] == 'NA':
                    stage.append(None)
                else:
                    stage.append(int(row[1]))

                if row[2] == 'NA':
                    count.append(None)
                else:
                    count.append(int(row[2]))

                if row[3] == 'NA':
                    rob_x.append(None)
                else:
                    rob_x.append(float(row[3]))

                if row[4] == 'NA':
                    rob_y.append(None)
                else:
                    rob_y.append(float(row[4]))

                if row[5] == 'NA':
                    rob_z.append(None)
                else:
                    rob_z.append(float(row[5]))
            
            data[name]['time_from_min'] = diff_time
            data[name]['stage'] = stage
            data[name]['count'] = count
            data[name]['rob_x'] = rob_x
            data[name]['rob_y'] = rob_y
            data[name]['rob_z'] = rob_z

    return data


def visualize_paths(data):
    for rob in data:
        stage = data[rob]['stage']
        x_pos = [-val for val in data[rob]['rob_x']]
        y_pos = data[rob]['rob_y']
        plt.scatter(y_pos, x_pos, label=rob)

        for i,(x,y) in enumerate(zip(y_pos, x_pos)):
            plt.annotate(stage[i],(x,y))
    ax = plt.gca()
    ax.set_aspect(1.0)
    plt.show()

def find_min_time(data):
    min_time = 9999999999
    for rob in data:
        time = data[rob]['time']
        x_pos = data[rob]['rob_x']
        y_pos = data[rob]['rob_y']
        start_time = time[0]

        if start_time < min_time:
            min_time = start_time
        
    return min_time

def diff_from_min_time(data, min_time):
    for rob in data:
        data[rob]['time_from_min'] = [round(time-min_time) for time in data[rob]['time']]
        # print(rob)
        # print(data[rob]['time_from_min'])

    return data

def process_diff_data(data, path):
    """
    Takes in data with diff from min time, and create a new
    dictionary with each of these times associated. Pads if it doesn't exist
    for another robot
    """

    out_dict = {}
    csv_columns = ['Diff Time', 'Stage', 'Count', 'X Pos', 'Y Pos', 'Z Pos', 'Actual Time']
    for rob in data:
        # out_dict[rob] = []
        stage = data[rob]['stage']
        count = data[rob]['count']
        x_pos = data[rob]['rob_x']
        y_pos = data[rob]['rob_y']
        z_pos = data[rob]['rob_z']
        actual_time = data[rob]['time']  
        diff_time = data[rob]['time_from_min']
        
        counter = 0.0
        out_path = path + str(rob) + '.csv'
        for i, time in enumerate(diff_time):
            with open(out_path, 'a') as csvfile:
                datawriter = csv.writer(csvfile, delimiter=',')
                if i == 0:
                    datawriter.writerow(csv_columns)
                # for some reason, the data hangs on 39 for a bit
                # if (diff_time[i] == counter) or (diff_time[i] == 39.0): 
                datawriter.writerow([diff_time[i], stage[i], count[i], x_pos[i], y_pos[i], z_pos[i], actual_time[i]])
                # else:
                #     datawriter.writerow([counter, 'NA', 'NA', 'NA', 'NA', 'NA', 'NA'])
                
            counter += 2.0

    print('CSV Robot Files Written')

def find_overlaps(data, dist=5.0):
    """
    Given robot data, find overlaps. Assumes data comes in stage aligned already

    parameters
    ----------
    data : dict
        Data processed for all robots

    dist : float
        Distance between points to be considered overlaps

    return
    ------

    overlaps : [o1, o2, ...]
        list of tuples of overlaps. Each tuple has the form (r1, r2,...) where the r value represents the
        stage that the robot overlaps with another robot 
    """

    overlaps = []
    rob_args = []
    for rob_id in data:
        rob_data = data[rob_id]
        rob_time_pos = zip(rob_data['new_stages'], rob_data['rob_x'], rob_data['rob_y'])
        rob_args.append(rob_time_pos)

    # generate product of all the points possible
    rob_pairs = itertools.product(*rob_args)
    for overlap_perm in rob_pairs:
        # each of the permutations may have more than 2 robots
        # need to check if any of these robots overlap
        # make 2-pairs since function only checks between 2 points

        skip = False
        for pairs in overlap_perm:
            if None in pairs:
                skip = True

        if not skip:
            overlap_comb = itertools.combinations(overlap_perm, 2)
            
            for rob_pair in overlap_comb:
                overlap_yes = in_overlap_dist(rob_pair[0], rob_pair[1], dist)
                if overlap_yes:
                    overlap = tuple([val[0] for val in rob_pair])
                    overlaps.append(overlap)
                    # print(rob_pair)
        else:
            skip = False
    return overlaps

def in_overlap_dist(tup_1, tup_2, thresh_dist):
    x1 = tup_1[1]
    y1 = tup_1[2]
    x2 = tup_2[1]
    y2 = tup_2[2]

    x_diff = x2 - x1
    y_diff = y2 - y1
    distance = np.sqrt(x_diff**2 + y_diff**2)
    if distance <= thresh_dist:
        return True
    else:
        return False

def sample_data(data, sample_rate, shift=0):
    for rob in data:
        for val in data[rob]:
            new_data = data[rob][val][shift::sample_rate]
            data[rob][val] = new_data
        # data[rob]['new_stages'] = [x+1 for x in range(len(data[rob]['count']))]
        data[rob]['new_stages'] = list(range(len(data[rob]['count'])))
    return data

def counts_to_array(data, round_val):
    counts = []
    for rob in data:
        for val in data[rob]:
            if val == 'count':
                counts.append(data[rob][val])
    arr_counts = np.array(counts, dtype=np.float)
    print('Average Counts: ', np.nanmean(arr_counts)/round_val)
    arr_counts = np.divide(arr_counts,round_val,where=arr_counts!=None)
    round_counts = np.rint(arr_counts, where=arr_counts!=None)
    print('Min Val: ', np.nanmin(round_counts))
    print('Max Val: ', np.nanmax(round_counts))

    return round_counts



if __name__ == "__main__":
    path = '/home/chris/grad_hw/marsupial_robotics/multi_SSAP/ssap_mcts/robot_paths/time_based/1x/*.txt'
    processed_csv_path = '/home/chris/grad_hw/marsupial_robotics/multi_SSAP/ssap_mcts/robot_paths/processed_csv_data/*.csv'
    out_file = '/home/chris/grad_hw/marsupial_robotics/multi_SSAP/ssap_mcts/misc_scripts/'

    # data = load_data(path)
    # min_time = find_min_time(data)
    # data = diff_from_min_time(data, min_time)
    # process_diff_data(data, out_file)

    # distance between points to be considered overlaps
    thresh_dist = 100.0
    sample_rate = 50
    round_vals = 600.0
    data = load_processed_data(processed_csv_path)
    data = sample_data(data, sample_rate)

    # overlaps and counts
    overlaps = find_overlaps(data, thresh_dist)
    out_counts_data = counts_to_array(data, round_vals)
    
    num_stages = out_counts_data.shape[1]
    num_overlaps = len(overlaps)
    print('Number of Stages: ', num_stages)
    print('Number of Overlaps: ', num_overlaps)
    
    # save count array, named with smapling rate
    course = 'alpha'
    count_filename = out_file + course + '_counts.npy'
    overlaps_filename = out_file + course + '_overlaps.npy'

    np.save(count_filename, out_counts_data)
    np.save(overlaps_filename, np.array(overlaps))



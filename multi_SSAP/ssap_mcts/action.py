import itertools
import numpy as np

class Action():
    def __init__(self, id, label):
        self.id = id
        self.label = label
        self.deploy_actions = None

    def toString(self):
        return str(self.label)

    def toInt(self):
        return int(self.label)

def printActionSequence(action_sequence):
    for action in action_sequence:
        print(action.toString() + ", "),
    print("")

def tolistActionSequence(action_sequence):
    actions = []
    for action in action_sequence:
        actions.append(action.toInt())
    return actions

def toActionActionSequence(action_list):
    num_stages = len(action_list)
    action_sequence = []
    for stage in range(num_stages):
        action_sequence.append(Action(stage, action_list[stage]))

    return action_sequence      

def tolistMultiActionSequence(action_sequence):
    if not action_sequence:
        return 0
    actions = []
    num_robots = len(action_sequence[0].deploy_actions)
    for i in range(num_robots):
        robot_actions = [action.deploy_actions[i] for action in action_sequence]
        actions.append(robot_actions)

    return actions

def toActionMultiActionSequence(action_list):
    num_stages = len(action_list[0])
    num_robots = len(action_list)
    action_sequence = []
    for stage in range(num_stages):
        new_action = Action(stage, stage)
        deploy_actions = np.zeros(num_robots)
        for rob in range(num_robots):
            deploy_actions[rob] = action_list[rob][stage]
        new_action.deploy_actions = tuple(deploy_actions)
        action_sequence.append(new_action)
    return action_sequence

class MultiRobotActions():
    def __init__(self, num_actions, num_ground_robots):
        self.num_robots = num_ground_robots
        # deploy or not
        self.num_total_actions = num_actions
        self.action_list = self._gen_robot_actions()

    def _gen_robot_actions(self):
        total_actions = []
        possible_deploy_actions = list(itertools.product(range(self.num_total_actions), repeat = self.num_robots))
        for i in range(self.num_total_actions ** self.num_robots):
            action = Action(id=i, label=i)
            action.deploy_actions = possible_deploy_actions[i]
            total_actions.append(action)

        return total_actions
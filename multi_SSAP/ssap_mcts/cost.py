from action import tolistActionSequence, tolistMultiActionSequence

import numpy as np

def cost(action_sequence):
    # A simple cost evaluation function
    return len(action_sequence)

def ssap_cost(action_sequence):
    stage_count = len(action_sequence)
    
    # deploys are 1, non-deploys 0
    list_actions = tolistActionSequence(action_sequence)
    deploy_count = list_actions.count(1)
    # print((stage_count, deploy_count))
    return (stage_count, deploy_count)

def ssap_cost_overbudget(sequence, total_budget):
    new_seq_cost = np.array(ssap_cost(sequence))
    # print('Seq Cost: ', new_seq_cost)
    # print('Cost Budget: ', total_budget) 
    diff = new_seq_cost - np.array(total_budget)
    # print('Diff: ', diff)
    return np.any(diff >= 0)

def ssap_multi_cost(action_sequence):
    stage_count = len(action_sequence)
    rob_cost = []
    # deploys are 1, non-deploys 0
    list_actions = tolistMultiActionSequence(action_sequence)
    for rob in list_actions:
        deploy_count = rob.count(1)
        rob_cost.append((stage_count, deploy_count))
    return rob_cost

def ssap_multi_cost_overbudget(sequence, total_budget):
    new_seq_costs = ssap_multi_cost(sequence)
    # print(np.array(tolistMultiActionSequence(sequence)))
    # print('Costs: ', new_seq_costs)
    robot_overbudget = []
    for i, rob_cost in enumerate(new_seq_costs): 
        new_seq_cost = np.array(rob_cost) 
        # print(new_seq_cost)
        diff = new_seq_cost - np.array([total_budget[0],total_budget[1][i]])
        robot_overbudget.append(np.any(diff > 0))
    # print(robot_overbudget)
    # print(np.any(robot_overbudget))

    return np.any(robot_overbudget)
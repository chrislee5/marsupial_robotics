"""
Chris Lee with help from Graeme Best's code

"""

import time, sys
import tqdm
import yaml
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import multiprocessing as mp

from datetime import date, datetime
from mcts import mcts, multi_mcts
from action import Action, MultiRobotActions, printActionSequence, toActionMultiActionSequence
from plot_tree import plotTree
from ssap import SSAP_Poisson
from reward import multi_ssap_reward
from functools import partial

from multiprocessing.pool import ThreadPool
from contextlib import contextmanager


np.set_printoptions(linewidth=200)

# ======== Some general run functions ========

def run():
    # total number of deploy actions. should be 2 for real tests
    num_actions = 2
    action_set = []
    for i in range(num_actions):
        id = i 
        action_set.append(Action(id,i))
    total_deploy = 3
    total_stages = 10
    overlap_paths = None
    # 0: normal, 1: ssap
    mcts_mode = 1

    # SSAP class
    poisson_mu = 5
    ssap = SSAP_Poisson(total_stages, poisson_mu)
    single_run_observations = ssap._gen_poisson_obs_vals(total_stages)

    # MCTS
    explore_param = 0.8 # =1.0 is recommended. <1.0 more exploitation. >1.0 more exploration. 
    max_iterations = 100000

    
    [solution, root, list_of_all_nodes, winner] = mcts(mcts_mode, action_set, total_stages, total_deploy, curr_obs, max_iterations, explore_param, ssap)

    print('Sol Length: ', len(solution))
    print('Total Nodes: ', len(list_of_all_nodes))
    print('Best Score: ', winner.avg_score)
    printActionSequence(solution)

    # plotTree(list_of_all_nodes, winner, action_set, False, total_stages, 1, explore_param)
    # plotTree(list_of_all_nodes, winner, action_set, True, total_stages, 2, explore_param)
    
    # print(list_of_all_nodes)
    # print('List Length: ', len(list_of_all_nodes))

def comparison_run():
    """
    Compare SSAP vs MCTS

    """
    comparison_iterations = 1
    total_deploy = 3
    total_stages = 10

    # SSAP class
    poisson_mu = 5
    ssap = SSAP_Poisson(total_stages, poisson_mu)

    # MCTS params
    mcts_params = {}
    mcts_params['max_iterations'] = 1000
    mcts_params['explore_param'] = 0.8 # =1.0 is recommended. <1.0 more exploitation. >1.0 more exploration. 


    total_ssap_rewards = []
    total_mcts_rand_rewards = []
    total_mcts_ssap_rewards = []
    total_random_rewards = []
    # do comparison iterations number of SSAP vs MCTS
    for comp_iter in tqdm.tqdm(range(comparison_iterations)):
        single_run_observations = ssap._gen_poisson_obs_vals(total_stages)
        oracle_reward = np.sum(single_run_observations[single_run_observations.argsort()][-total_deploy:])

        # Debug
        # print('Obs:', single_run_observations)
        # print(oracle_reward)

        # Do SSAP 
        ssap_reward = ssap_run(single_run_observations, oracle_reward, total_deploy, total_stages, ssap)
        total_ssap_rewards.append(ssap_reward)

        # Do MCTS - Random Rollout
        mcts_params['mcts_mode'] = 0
        mcts_rand_reward = mcts_run(single_run_observations, oracle_reward, total_deploy, total_stages, ssap, mcts_params)
        total_mcts_rand_rewards.append(mcts_rand_reward)

        # # Do MCTS - SSAP Rollout
        mcts_params['mcts_mode'] = 1
        mcts_ssap_reward = mcts_run(single_run_observations, oracle_reward, total_deploy, total_stages, ssap, mcts_params)
        total_mcts_ssap_rewards.append(mcts_ssap_reward)

        # Do Random
        random_reward = random_run(single_run_observations, oracle_reward, total_deploy)
        total_random_rewards.append(random_reward)

    fig, ax = plt.subplots()
    labels = ['SSAP','MCTS','Random']
    ax.boxplot([total_ssap_rewards, total_mcts_rand_rewards, total_mcts_ssap_rewards, total_random_rewards])
    ax.set(xticklabels=['SSAP', 'MCTS-Random', 'MCTS-SSAP', 'Random'])
    plt.show()

def multi_comparison_run():
    """
    Compare SSAP vs MCTS

    """
    # Load config
    config = load_config()

    # Test params
    test_params = config['test_params']
    comparison_iterations = test_params['comparison_iterations']
    total_deploy = test_params['total_deploy']
    total_ground_robots = test_params['total_ground_robots']
    total_stages = test_params['total_stages']
    overlaps = test_params['overlaps']
    total_experiments = test_params['algorithms']

    # SSAP class
    ssap_params = config['ssap_params']
    poisson_mu = ssap_params['poisson_mu']
    ssap = SSAP_Poisson(total_stages, poisson_mu)
    ssap.total_drone_deploys = total_deploy

    # MCTS params
    mcts_params = config['mcts_params']

    total_ssap_rewards = []
    total_mcts_rand_rewards = []
    total_mcts_ssap_rewards = []
    total_random_rewards = []

    # Saveout array
    save_action_arr = np.zeros((len(total_experiments),comparison_iterations, total_ground_robots, total_stages))
    save_obs_arr = np.zeros((comparison_iterations, total_ground_robots, total_stages))
    save_path = None
    # do comparison iterations number of SSAP vs MCTS
    for comp_iter in tqdm.tqdm(range(comparison_iterations)):
        multi_rob_obs = []
        total_oracle_reward = 1.0
        for i in range(total_ground_robots):
            single_run_observations = ssap._gen_poisson_obs_vals(total_stages)
            oracle_reward = np.sum(single_run_observations[single_run_observations.argsort()][-total_deploy:])
            multi_rob_obs.append(single_run_observations)

        oracle_reward =  multi_oracle_reward(multi_rob_obs, total_deploy)

        # # Do SSAP 
        ssap_actions = multi_ssap_run(multi_rob_obs, oracle_reward, total_ground_robots, 
                                    total_deploy, total_stages, ssap, overlaps)
        ssap_reward = calc_penalty_reward(multi_rob_obs, overlaps, ssap_actions, 
                                            total_stages, total_ground_robots, total_deploy, ssap)     
        total_ssap_rewards.append(ssap_reward/float(oracle_reward))

        # Do MCTS - Random Rollout
        mcts_rand_actions = multi_mcts_run(multi_rob_obs, oracle_reward, total_ground_robots,
                                         total_deploy, total_stages, ssap, overlaps, mcts_params)
        mcts_rand_reward = calc_penalty_reward(multi_rob_obs, overlaps, mcts_rand_actions, 
                                            total_stages, total_ground_robots, total_deploy, ssap)
        total_mcts_rand_rewards.append(mcts_rand_reward/float(oracle_reward))

        
        # save
        save_action_arr[0,comp_iter] = ssap_actions
        save_action_arr[1,comp_iter] = mcts_rand_actions
        save_obs_arr[comp_iter] = np.array(multi_rob_obs)
        save_path = save_data(save_action_arr, save_obs_arr, config, save_path)

    fig, ax = plt.subplots()
    ax.boxplot([total_ssap_rewards, total_mcts_rand_rewards])
    ax.set(xticklabels=total_experiments)
    plt.savefig(save_path + 'comparison.eps', format='eps')

def real_data_multi_comparison_run():
    """
    Compare SSAP vs MCTS

    """
    # Load config
    config = load_config(cfg='config/real_data_multi_ssap_config.yaml')

    # Test params
    test_params = config['test_params']
    comparison_iterations = test_params['comparison_iterations']
    total_deploy = test_params['total_deploy']
    total_ground_robots = test_params['total_ground_robots']
    total_stages = test_params['total_stages']
    total_experiments = test_params['algorithms']
    count_data_loc = test_params['count_data_loc']
    overlaps_loc = test_params['overlaps_loc']

    # SSAP class
    ssap_params = config['ssap_params']
    poisson_mu = ssap_params['poisson_mu']
    ssap = SSAP_Poisson(total_stages, poisson_mu)
    ssap.total_drone_deploys = total_deploy

    # MCTS params
    mcts_params = config['mcts_params']

    total_ssap_rewards = []
    total_mcts_rand_rewards = []
    total_mcts_ssap_rewards = []
    total_random_rewards = []

    # Saveout array
    save_action_arr = np.zeros((len(total_experiments),comparison_iterations, total_ground_robots, total_stages))
    save_obs_arr = np.zeros((comparison_iterations, total_ground_robots, total_stages))
    save_path = None

    # count data. assume comes in rounded
    count_data = np.load(count_data_loc)
    overlaps = np.load(overlaps_loc)

    # do comparison iterations number of SSAP vs MCTS
    for comp_iter in tqdm.tqdm(range(comparison_iterations)):
        multi_rob_obs = [list(counts) for counts in count_data]
        total_oracle_reward = 1.0

        oracle_reward = multi_oracle_reward(multi_rob_obs, total_deploy)

        # # Do SSAP 
        ssap_actions = multi_ssap_run(multi_rob_obs, oracle_reward, total_ground_robots, 
                                    total_deploy, total_stages, ssap, overlaps)
        ssap_reward = calc_penalty_reward(multi_rob_obs, overlaps, ssap_actions, 
                                            total_stages, total_ground_robots, total_deploy, ssap)     
        total_ssap_rewards.append(ssap_reward/float(oracle_reward))

        # Do MCTS - Random Rollout
        mcts_rand_actions = multi_mcts_run(multi_rob_obs, oracle_reward, total_ground_robots,
                                         total_deploy, total_stages, ssap, overlaps, mcts_params)
        mcts_rand_reward = calc_penalty_reward(multi_rob_obs, overlaps, mcts_rand_actions, 
                                            total_stages, total_ground_robots, total_deploy, ssap)
        total_mcts_rand_rewards.append(mcts_rand_reward/float(oracle_reward))

        
        # save
        save_action_arr[0,comp_iter] = ssap_actions
        save_action_arr[1,comp_iter] = mcts_rand_actions
        save_obs_arr[comp_iter] = np.array(multi_rob_obs)
        save_path = save_data(save_action_arr, save_obs_arr, config, save_path)

    fig, ax = plt.subplots()
    ax.boxplot([total_ssap_rewards, total_mcts_rand_rewards])
    ax.set(xticklabels=total_experiments)
    plt.savefig(save_path + 'comparison.eps', format='eps')


# ======== Multi Robot with Multiprocessing ========

def mp_multi_calc_alg(comp_iter, config, ssap, save_path):
    # time
    # print('Process ID: ', os.getpid())
    print('Iteration: ', comp_iter)
    start = time.time()

    # get parameters from config dict
    test_params = config['test_params']
    total_ground_robots = test_params['total_ground_robots']
    total_stages = test_params['total_stages']
    total_deploy = test_params['total_deploy']
    overlaps = test_params['overlaps']

    mcts_params = config['mcts_params']

    multi_rob_obs = []
    for i in range(total_ground_robots):
        single_run_observations = ssap._gen_poisson_obs_vals(total_stages)
        oracle_reward = np.sum(single_run_observations[single_run_observations.argsort()][-total_deploy:])
        multi_rob_obs.append(single_run_observations)

    oracle_reward = multi_oracle_reward(multi_rob_obs, total_deploy)

    # # Do SSAP 
    ssap_actions = multi_ssap_run(multi_rob_obs, oracle_reward, total_ground_robots, 
                                total_deploy, total_stages, ssap, overlaps)
    ssap_reward = calc_penalty_reward(multi_rob_obs, overlaps, ssap_actions, 
                                        total_stages, total_ground_robots, total_deploy, ssap)     

    # Do MCTS - Random Rollout
    mcts_rand_actions = multi_mcts_run(multi_rob_obs, oracle_reward, total_ground_robots,
                                        total_deploy, total_stages, ssap, overlaps, mcts_params)
    mcts_rand_reward = calc_penalty_reward(multi_rob_obs, overlaps, mcts_rand_actions, 
                                        total_stages, total_ground_robots, total_deploy, ssap)

    mp_save_data(comp_iter, save_path, multi_rob_obs,ssap_actions,mcts_rand_actions)

    end = time.time()
    # print('1 Test Time: ', end-start)

    return (oracle_reward, ssap_reward, mcts_rand_reward)

def multiprocess_multi_run():
    """
    Compare SSAP vs MCTS

    """

    # Load config
    config = load_config()

    # Create save path
    save_path = mp_save_path(config)


    # Test params
    test_params = config['test_params']
    comparison_iterations = test_params['comparison_iterations']
    total_stages = test_params['total_stages']
    total_experiments = test_params['algorithms']

    # SSAP class
    ssap_params = config['ssap_params']
    poisson_mu = ssap_params['poisson_mu']
    ssap = SSAP_Poisson(total_stages, poisson_mu)
    ssap.total_drone_deploys = test_params['total_deploy']

     
    # do comparison iterations number of SSAP vs MCTS     
    print('CPU Count: ', mp.cpu_count())
    
    pool = mp.Pool(processes=mp.cpu_count()-1)
    mp_calc_alg = partial(mp_multi_calc_alg, config=config, ssap=ssap, save_path=save_path)
    results = [pool.apply_async(mp_calc_alg, args=(x,)) for x in range(comparison_iterations)]
    # result = pool.map(multi_calc_alg, range(comparison_iterations))

    output = []
    for jobs in tqdm.tqdm(results):
        output.append(jobs.get())
    # output = [p.get() for p in results]

    mp_plot_result(output, save_path, total_experiments)
    print('Results')
    with open(save_path + '/results.npy', 'wb') as f:
        np.save(f, np.array(output))
    
def save_data(save_arr, obs_arr, config, save_path):
    """
    Saves out the results with hopefully all the config information

    """
    today = date.today()
    curr_date = today.strftime("%b-%d-%Y")
    # make it if it doesn't exist
    path = 'test_results/' + curr_date
    if not os.path.exists(path):
        os.makedirs(path)
    
    # first loop through
    if not save_path:
        # create current test path
        now = datetime.now()
        time_str = now.strftime("%H:%M:%S")
        save_path = path + '/' + time_str + '/'
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        # save config
        with open(save_path+'config.yaml','w') as outfile:
            yaml.dump(config, outfile)
    
    with open(save_path + 'results.npy', 'wb') as f:
        np.save(f, save_arr)

    with open(save_path + 'obs.npy', 'wb') as f:
        np.save(f, obs_arr)

    return save_path

def mp_save_path(config):
    """
    Saves out the results with hopefully all the config information

    """
    today = date.today()
    curr_date = today.strftime("%b-%d-%Y")
    # make it if it doesn't exist
    path = 'test_results/' + curr_date
    if not os.path.exists(path):
        os.makedirs(path)
    
    # create current test path
    now = datetime.now()
    time_str = now.strftime("%H:%M:%S")
    save_path = path + '/' + time_str + '/'
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    # save config
    with open(save_path+'config.yaml','w') as outfile:
        yaml.dump(config, outfile)
    
    return save_path

def mp_save_data(test_iter, path, obs, ssap_actions, mcts_rand_actions):
    save_path = path + '/' + str(test_iter)
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    
    with open(save_path + '/obs.npy', 'wb') as f:
        np.save(f, obs)
    with open(save_path + '/ssap_actions.npy', 'wb') as f:
        np.save(f, ssap_actions)
    with open(save_path + '/mcts_rand_actions.npy', 'wb') as f:
        np.save(f, mcts_rand_actions)

def mp_plot_result(results, save_path, total_experiments):
    """
    Result is a list of tuples (obs, ssap, mcts)

    """
    total_ssap_rewards = []
    total_mcts_rand_rewards = []

    for result in results:
        oracle = result[0]
        ssap = result[1]
        mcts = result[2]
        total_ssap_rewards.append(ssap/oracle)
        total_mcts_rand_rewards.append(mcts/oracle)

    fig, ax = plt.subplots()
    ax.boxplot([total_ssap_rewards, total_mcts_rand_rewards])
    ax.set(xticklabels=total_experiments)
    plt.savefig(save_path + 'comparison.eps', format='eps')

def load_config(cfg='config/multi_ssap_config.yaml'):
    """
    Loads config from the yaml file
    """
    # cfg = 'config/multi_ssap_config.yaml'
    with open(cfg) as f:
        config = yaml.load(f)
    return config

def check_data():
    data = np.load('test_results/Feb-24-2021/14:32:18/results.npy')
    print(data)
    obs = np.load('test_results/Feb-24-2021/14:32:18/obs.npy')
    print(obs)           


# ======== Single Robot ========
def mcts_run(obs, oracle_reward, total_deploy, total_stages, ssap, mcts_params):
    """
    Takes in a single set of observations and returns the reward from MCTS approach

    """
    mcts_reward = []
    num_actions = 2
    action_set = []
    for i in range(num_actions):
        id = i 
        action_set.append(Action(id,i))

    max_iterations = mcts_params['max_iterations']
    explore_param = mcts_params['explore_param']
    mcts_mode = mcts_params['mcts_mode']

    mcts_deploys = total_deploy
    for stage in range(total_stages): 
        curr_obs = obs[stage]
        stages_remaining = total_stages - stage
        # DEBUG
        # print('Stages: ', stages_remaining)
        # print('MCTS Deploys: ', mcts_deploys)
        
        # Is it right if we skip planning entirely if we have 0 deploys? Seems right
        if mcts_deploys is not 0:
            [solution, root, list_of_all_nodes, winner] = mcts(0, action_set, stages_remaining, curr_obs, 
                                                                mcts_deploys, max_iterations, explore_param, ssap)
            best_action = solution[0]
            # deploy action
            if best_action.id == 1:
                # print('Deploys')
                mcts_reward.append(curr_obs)
                mcts_deploys -= 1
                if mcts_deploys <= 0:
                    mcts_deploys = 0
        print('Explored Nodes: ', len(list_of_all_nodes))
    # print(mcts_reward)
    return float(np.sum(mcts_reward))/float(oracle_reward)

def ssap_run(obs, oracle_reward, total_deploy, total_stages, ssap):
    thresholds = ssap.thresholds
    current_stage = 1
    ssap_deploys = 0
    ssap_reward = []
    while ssap_deploys < total_deploy:
        deploys_remaining = total_deploy - ssap_deploys
        stages_remaining = total_stages - current_stage 
        
        current_obs = obs[current_stage - 1]
        stage_threshold = np.trim_zeros(thresholds[:,-current_stage], trim='b')
        thresh_val = stage_threshold[-(deploys_remaining+1)]

        if (current_obs > thresh_val) or (deploys_remaining == stages_remaining):
            ssap_reward.append(current_obs)
            ssap_deploys += 1

        current_stage += 1

    # print(ssap_reward)
    return float(np.sum(ssap_reward))/float(oracle_reward)

def random_run(obs, oracle_reward, total_deploy):
    random_choices = np.random.choice(obs, size=total_deploy, replace=False)
    return float(np.sum(random_choices))/float(oracle_reward)


# ======== Multi Robot ========
def multi_oracle_reward(multi_obs, total_deploy):
    total_reward = 0
    for single_run_observations in multi_obs:
        oracle_reward = np.sum(single_run_observations[single_run_observations.argsort()][-total_deploy:])
        total_reward += oracle_reward
    return total_reward

def multi_mcts_run(obs, oracle_reward, total_ground_robots, total_deploy, total_stages, ssap, overlaps, mcts_params):
    """
    Takes in a single set of observations and returns the reward from MCTS approach

    """
    mcts_reward = []
    robot_actions = np.zeros((total_ground_robots, total_stages))
    num_actions = 2
    action_class = MultiRobotActions(num_actions, total_ground_robots)
    action_set = action_class.action_list
    # print('Num Actions: ', len(action_set))
    max_iterations = mcts_params['max_iterations']
    explore_param = mcts_params['explore_param']
    mcts_mode = mcts_params['mcts_mode']

    robot_deploy_budgets = np.ones(total_ground_robots) * total_deploy 
    total_robots = total_deploy * total_ground_robots

    # print('Observations: ')
    # print(np.array(obs))
    curr_obs = np.zeros(total_ground_robots)
    for stage in range(total_stages):
        for i in range(total_ground_robots): 
            curr_obs[i] = obs[i][stage]
        stages_remaining = total_stages - stage
        # DEBUG
        # print('Stages: ', stages_remaining)
        # print('============================')
        # print('MCTS Deploys: ', mcts_deploys)
        # print('Budget: ', robot_deploy_budgets)
        # Is it right if we skip planning entirely if we have 0 deploys? Seems right
        past_obs = robot_actions*obs
        
        # send this into ssap class
        ssap_adaptive_idx = gen_overlap_idx(overlaps, robot_actions) 
        ssap.adaptive_thresholds = ssap.gen_DRL_adaptive_thresh(n_stage=total_stages, overlap_idx=ssap_adaptive_idx)
                
        if not np.all(robot_deploy_budgets == 0):
            [solution, root, list_of_all_nodes, winner] = multi_mcts(mcts_mode, action_set, stages_remaining, curr_obs, 
                                                                robot_deploy_budgets, max_iterations, explore_param, 
                                                                ssap, overlaps, past_obs, total_robots)
            best_action = solution[0]
            # deploy actions
            deploy_actions = best_action.deploy_actions
            for i, rob_deploy in enumerate(deploy_actions):
                # robot id i deploys
                if rob_deploy == 1:
                    robot_deploy_budgets[i] -= 1
                    robot_actions[i, stage] = 1

                    mcts_reward.append(curr_obs[i])
                    # may need something here to limit the deploy budget to be 0 and not negative

        # print('Multi Total Nodes Explored: ', len(list_of_all_nodes))
        # plotTree(list_of_all_nodes, winner, action_set, True, total_stages, 1, explore_param)
        # print('')
    # print(mcts_reward)
    return robot_actions

def multi_ssap_run(obs, oracle_reward, total_ground_robots, total_deploy, total_stages, ssap, overlaps):
    thresholds = ssap.thresholds
    action_list = []
    for rob_id, rob_obs in enumerate(obs):
        current_stage = 1
        ssap_deploys = 0
        ssap_reward = []
        ssap_no_penalty = []
        rob_actions = []
        while ssap_deploys < total_deploy:
            deploys_remaining = total_deploy - ssap_deploys
            stages_remaining = total_stages - current_stage 
            
            current_obs = obs[rob_id][current_stage - 1]
            stage_threshold = np.trim_zeros(thresholds[:,-current_stage], trim='b')
            thresh_val = stage_threshold[-(deploys_remaining+1)]

            if (current_obs > thresh_val) or (deploys_remaining == stages_remaining):
                ssap_reward.append(current_obs)
                ssap_deploys += 1
                rob_actions.append(1)
            else:
                rob_actions.append(0)
            current_stage += 1
        pad_val = total_stages - len(rob_actions)
        rob_actions += [0] * (pad_val)
        action_list.append(rob_actions)
    # action_sequence = toActionMultiActionSequence(action_list)

    # total_budget = (total_stages, total_deploy)
    # # this reward comes out normalized. Need to un-normalize
    # ssap_reward = multi_ssap_reward(action_sequence, obs, total_budget, ssap, overlaps, None)
    # ssap_reward = ssap_reward * total_budget[1] * ssap.poisson_normalization_nintey * total_ground_robots

    # # calculate the unpenalized rewards
    # action_mask = np.ma.make_mask(action_list)
    # deployed_obs = obs*action_mask

    # print('SSAP Unpenalized: ', np.sum(deployed_obs))
    # print('SSAP Penalized: ', ssap_reward)
    # print(np.array(obs))
    # print(np.array(action_list))
    return np.array(action_list)

def calc_penalty_reward(obs, overlaps, action_list, total_stages, total_ground_robots, total_deploy, ssap):
    action_sequence = toActionMultiActionSequence(action_list)
    # this reward comes out normalized. Need to un-normalize
    total_budget = (total_stages, total_deploy)
    reward = multi_ssap_reward(action_sequence, obs, total_budget, ssap, overlaps, None, total_ground_robots*total_deploy)
    reward = reward * total_deploy * ssap.poisson_normalization_nintey * total_ground_robots

    return reward

def gen_overlap_idx(overlaps, robot_actions):

    """
    parameters
    ----------
    
    overlaps : [(o1, o2, ...)]
        list of tuples of overlaps. Each tuple has the form (r1, r2,...) where the r value represents the
        stage that the robot overlaps with another robot

    robot_actions: np.array((total_ground_robots, total_stages))
        Total ground robots x total stages

    
    returns
    -------
    overlap_idx : {robot_id : [(s,p)]}
        S = stage where robot has the overlap(s) and P = penalty, so number of overlaps
    
    """

    overlap_idx = {}

    # for each robot and their current actions, find which robot will have future overlaps 
    for rob_id, rob_actions in enumerate(robot_actions):
        rob_overlap_idx = {}

        # check each overlap, to see if the robot has an overlap stage ahead of other robots
        for overlap in overlaps:
            if overlap[rob_id] is not None:
                curr_rob_stage = overlap[rob_id]
            # robot has no overlap
            else:
                continue

            stage_penalty = (curr_rob_stage,0)
            
            # check each robot's overlap stage
            for overlap_rob_id, overlap_val in enumerate(overlap):
                # handles the same robot situation
                if rob_id == overlap_rob_id:
                    continue

                # the robot stage is ahead of (or at) other robot's overlap stage
                if overlap_val is not None and curr_rob_stage >= overlap_val:
                    # check if that robot's stage has a deploy action in robot actions
                    other_rob_action = robot_actions[overlap_rob_id][overlap_val]

                    # other robot deploys 
                    if other_rob_action == 1:
                        stage_penalty = (stage_penalty[0], stage_penalty[1]+1)
            
            if stage_penalty[1] is not 0:
                if stage_penalty[0] not in rob_overlap_idx:
                    rob_overlap_idx[stage_penalty[0]] = stage_penalty[1]
                else:
                    rob_overlap_idx[stage_penalty[0]] += stage_penalty[1]
                
        add_idx = [(key,rob_overlap_idx[key]) for key in rob_overlap_idx]
       
        overlap_idx[rob_id] = add_idx

    return overlap_idx


if __name__ == "__main__":
    start = time.time()
    # run()
    # comparison_run()
    # multi_comparison_run()
    multiprocess_multi_run()
    # check_data()
    # test_run()
    end = time.time()
    print('Time: ', (end-start)/3600.0)
    

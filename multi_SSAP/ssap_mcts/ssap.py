import scipy.stats as stats
import numpy as np
import tqdm
import math

class SSAP_base():
    def __init__(self):
        np.set_printoptions(linewidth=200)
        pass

    def _sum_term(self,func,lower,upper,**kwargs):
        val = 0
        # print('Lower Upper')
        # print(lower,upper)
        # print('Range')
        # print(int(upper-lower))
        for i in range(int(upper-lower)+1):
            k = i+lower
            kwargs['k'] = k
            # print(k)
            val += func(**kwargs)
        return val

    def _print_thresholds(self, thresholds):
        round_threshold = 3
        print(np.round(thresholds,round_threshold))
        # print('Array: ',np.array2string(thresholds, max_line_width=200, precision=round_threshold))


class SSAP_Poisson(SSAP_base):

    def __init__(self, total_stages, poisson_lambda):
        self.total_stages = total_stages

        # Poisson distribution parameters
        self.lamb = poisson_lambda
        self.shift = 0 
        self.upperbound = 100
 
        # MCTS normalization values
        self.poisson_normalization_nintey = self.gen_poisson_normalization_vals(self.lamb)    

        self.thresholds = np.round(self._gen_DRL_thresh(n_stage=self.total_stages), 3)
        # need to init this from outside the class
        self.adaptive_thresholds = None

        # MCTS deploy values
        self.total_drone_deploys = 0
        # [s1, s2, ...]
        self.current_stage = 0
        self.robot_true_stage = None
        self.real_obs = None
    
    def gen_poisson_normalization_vals(self, dist_lam):
        ninety_percent_val = 0
        while stats.poisson.cdf(ninety_percent_val, dist_lam) < 0.9:
            ninety_percent_val += 1

        return ninety_percent_val
    
    def gen_DRL_adaptive_thresh(self, n_stage=10, overlap_idx=None, stage_counts=None):
        """
        parameter
        ---------
        n_stage : int
            Number of stages to calculate

        overlap_idx : {rob_id: [(s,p), ...], ... }
            Dict. Robot ID : list of tuples where s = stages and p = penalty amount (divides lambda)
        
        stage_counts : [s1, s2, ....]
            Tuple of counts of total stages per robot. Due to NaNs

        returns
        -------
        robot_thresholds : {rob_id : SSAP thresholds}
            SSAP threshold per robot

        """
        robot_thresholds = {}

        # generate SSAP thresholds for each robot
        for rob_id in overlap_idx:
            if stage_counts is not None:
                n_stage = stage_counts[rob_id]
            # convert overlap_idx into stages remaining (how the thresholds work), instead of current stage
            new_idx = [(n_stage-tup[0]-1,tup[1]) for tup in overlap_idx[rob_id]]
            overlaps = dict(new_idx)
            # print('Old: ', overlap_idx[rob_id])
            # print('New: ', new_idx)
            # print('Rob ID: ', rob_id)
            thresh = np.zeros((n_stage+1,n_stage))
            for i in range(n_stage):
                thresh[i+1,i] = self.upperbound
                lamb = self.lamb

                # adjust new lambda. Divide by penalty amount plus one, due to minimum of 1 penalty
                if i in overlaps:
                    lamb /= float(overlaps[i]) + 1 
                    # print('Penalty: ', overlaps[i])
                
                # if rob_id == 0:
                #     print('Stage: ', i)
                #     print('Lamb: ', lamb)
                for j in range(i+1):
                    if j == 0:
                        continue
                    else:
                        # indexing is prob j-row then stage i-column
                        # lower bound, a_i-1,n-1
                        low = math.floor(thresh[j-1,i-1])
                        lowKwarg = {'k':math.floor(thresh[j-1,i-1]),
                                    'lam':lamb,
                                    'shift':self.shift}
                        
                        low_cdf = self._poisson_cdf(**lowKwarg)
                        second = thresh[j-1,i-1]*low_cdf

                        # upper bound, a_i,n-1
                        upper = math.floor(thresh[j,i-1])
                        highKwarg = {'k':upper,
                                    'lam':lamb,
                                    'shift':self.shift}
                        high_cdf = self._poisson_cdf(**highKwarg)
                        third = thresh[j,i-1]*(1-high_cdf)

                        firstKwarg = {'lam':lamb,
                                    'shift':self.shift}
                        first = self._sum_term(self._xDotPoisson,low+1,upper,**firstKwarg)
                        aThresh = first + second + third
                        
                        thresh[j,i] = aThresh

            robot_thresholds[rob_id] = np.round(thresh,2)
    
        return robot_thresholds

    def _gen_poisson_obs_vals(self, stages):
        obs_vals = stats.poisson.rvs(self.lamb, 0, stages)
        return obs_vals

    def _gen_multi_poisson_obs_vals(self, ground_robots, stages):
        total_obs = []
        for i in range(ground_robots):
            obs_vals = stats.poisson.rvs(self.lamb, 0, stages)
            total_obs.append(obs_vals)
        return total_obs

    def _gen_DRL_thresh(self,n_stage=10):
        # print('===================== Starting DRL Thresh =====================')
        thresh = np.zeros((n_stage+1,n_stage))
        # for i in tqdm.tqdm(range(n_stage)):
        for i in range(n_stage):
            thresh[i+1,i] = self.upperbound
            # prob
            for j in range(i+1):
                if j == 0:
                    continue
                else:
                    # indexing is prob j-row then stage i-column
                    # lower bound, a_i-1,n-1
                    low = math.floor(thresh[j-1,i-1])
                    lowKwarg = {'k':math.floor(thresh[j-1,i-1]),
                                'lam':self.lamb,
                                'shift':self.shift}
                    
                    low_cdf = self._poisson_cdf(**lowKwarg)
                    second = thresh[j-1,i-1]*low_cdf

                    # upper bound, a_i,n-1
                    upper = math.floor(thresh[j,i-1])
                    highKwarg = {'k':upper,
                                 'lam':self.lamb,
                                 'shift':self.shift}
                    high_cdf = self._poisson_cdf(**highKwarg)
                    third = thresh[j,i-1]*(1-high_cdf)

                    firstKwarg = {'lam':self.lamb,
                                 'shift':self.shift}
                    first = self._sum_term(self._xDotPoisson,low+1,upper,**firstKwarg)
                    aThresh = first + second + third
                    
                    thresh[j,i] = aThresh

        return thresh

    def _poisson_cdf(self,**kwargs):
        k = kwargs['k']
        lam = kwargs['lam']
        shift = kwargs['shift']
        return stats.poisson.cdf(k,mu=lam,loc=shift)

    def _xDotPoisson(self,**kwargs):
        k = kwargs['k']
        lam = kwargs['lam']
        shift = kwargs['shift']
        # should this be floor??
        return k*stats.poisson.pmf(math.floor(k),mu=lam,loc=shift)

 
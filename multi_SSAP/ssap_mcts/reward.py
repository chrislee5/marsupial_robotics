from action import tolistActionSequence, tolistMultiActionSequence

import numpy as np
import scipy.stats as stats
import sys

def simple_reward(action_sequence):
    # A simple reward function

    # Iterate through the sequence, looking at pairs
    reward = 0
    for i in range(len(action_sequence)-1): # Yes, we want -1 here
        
        # Pick out a pair
        first = action_sequence[i]
        second = action_sequence[i+1]

        # Add to the reward if second is +1
        if first.id + 1 == second.id:
            reward += 1

    # Also give reward for first action by itself
    if action_sequence[0].id == 1:
        reward += 1

    # Normalise between 0 and 1
    max_reward = len(action_sequence) #-1
    if max_reward == 0:
        reward_normalised = 0
    else:
        reward_normalised = float(reward) / float(max_reward)
    
    # print('Reg Reward Normalized: ', reward_normalised)
    return reward_normalised

def ssap_reward(action_sequence, obs_vals, budget, ssap):
    actions = tolistActionSequence(action_sequence)
    # print(actions)
    # print('Obs Val: ', obs_vals)
    # print('Length: ', len(action_sequence))
    action_mask = np.ma.make_mask(actions)
    deployed_vals = np.array(obs_vals)[:len(actions)][action_mask]
    ninety_percent_val = ssap.poisson_normalization_nintey

    # try to normalize by dividing by the 90%, or around there, cdf value
    deployed_vals = deployed_vals/float(ninety_percent_val)

    # normalize close to 0 to 1 for reward by dividing by total deploy robots
    total_vals = np.sum(deployed_vals)/budget[1]

    # print('SSAP Reward Normalized: ', total_vals)
    return total_vals

def multi_ssap_reward(action_sequence, obs_vals, budget, ssap, overlaps, past_obs, total_robots):
    num_carrier_robots = len(action_sequence[0].deploy_actions)
    past_stages = ssap.total_stages - len(action_sequence)
    # return a list of lists of all the robot actions
    robot_actions = tolistMultiActionSequence(action_sequence)
    robot_actions = np.stack(robot_actions, axis=0)
    obs_vals = np.stack(obs_vals, axis=0)

    action_mask = np.ma.make_mask(robot_actions)
    deployed_obs = obs_vals*action_mask
    # # print('Stages Remaining: ', stages_remaining)
    # padded_deployed_obs = np.pad(deployed_obs, ((0,0),(past_stages,0)), mode='constant')

    if past_obs is not None:
        padded_deployed_obs = np.append(past_obs[:,0:past_stages], deployed_obs, axis=1)
        check_past_obs = past_obs[:,0:past_stages]

    else:
        padded_deployed_obs = deployed_obs
        check_past_obs = np.zeros((num_carrier_robots,past_stages))

    padded_deployed_obs = np.nan_to_num(padded_deployed_obs)
    # print(padded_deployed_obs)
    # print(np.nan_to_num(padded_deployed_obs))

    penalized_deployed_obs = calc_overlap_norm_penalty(padded_deployed_obs, num_carrier_robots, overlaps)

    ninety_percent_val = ssap.poisson_normalization_nintey

    # print(np.sum(penalized_deployed_obs))
    # try to normalize by dividing by the 90%, or around there, cdf value
    penalized_deployed_obs = penalized_deployed_obs/float(ninety_percent_val)

    # normalize close to 0 to 1 for reward by dividing by total deploy robots
    robot_reward = np.sum(penalized_deployed_obs)/total_robots
    return robot_reward

def calc_overlap_norm_penalty(deployed_obs, num_carrier_robots, overlaps):
    # higher penalty term really hurts the overlaps
    penalty_term = 1

    ground_rob = num_carrier_robots
    stages = len(deployed_obs[0])
    penalty = np.ones((ground_rob, stages))
    if not overlaps:
        return deployed_obs

    for overlap in overlaps:
        overlap_mask = np.zeros((ground_rob, stages))
        for rob_id, rob in enumerate(overlap):
            # print('Rob_ID: ', rob_id)
            # print('Rob: ', rob)
            if rob is not None:
                overlap_mask[rob_id,rob] = 1
        overlap_mask = np.ma.make_mask(overlap_mask)
        overlap_deployed_obs = deployed_obs*overlap_mask
        total_overlaps = np.count_nonzero(overlap_deployed_obs)
        
        # If at least two robots overlap
        if total_overlaps > 1:
            penalty += (overlap_mask*penalty_term)
    # print(penalty)
    # print('')

    # print(deployed_obs)
    # print(deployed_obs/penalty)
    
    return (deployed_obs/penalty)

def check_rewards():
    return

def check_multi_rewards(past_obs, current_actions, ssap):
    past_obs_counts = np.count_nonzero(past_obs, axis=1)
    curr_action_counts = np.count_nonzero(current_actions, axis=1)
    counts = past_obs_counts + curr_action_counts
    if not np.all(counts==ssap.total_drone_deploys):
        print('Rewards do not match deploy count')
        print('Counts: ', counts)
        print('Deploys: ', ssap.total_drone_deploys)
        
        return False
    print('')
    return True
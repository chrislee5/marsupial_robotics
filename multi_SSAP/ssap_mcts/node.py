"""
Chris Lee with help from Graeme Best's code

"""

import numpy as np
import scipy.stats as stats

class Node():

    # single robot MCTS case
    _total_obs_val = None

    def __init__(self, parent, sequence, stage_id, budget, unpicked_children, **kwargs):
        """
        parent : Node
            Node type of parent

        unpicked_children : [actions]
            List of actions to take

        sequence : [actions]
            List of historical actions that leads to this node

        stage_id : int
            Current stage

        budget : (int, int)
            (Total stages, number of drones left to deploy)

        ==== kwargs ====
        num robots : int
            Number of ground robots
        
        poisson_mu : int
            Distribution info

        total_stages : int
            Total number of stages


        """

        # node properties
        self.parent = parent
        self.children = []
        self.action_id = None
        self.stage_id = stage_id
        self.unpicked_children = unpicked_children

        # historical sequence information
        self.sequence = sequence
        self.stage_budget = budget[0]
        self.deploy_budget = budget[1]

        # reward info
        self.avg_score = 0
        self.update_count = 0

        # DEBUG
        # print(self.observation_val)

    def updateAverage(self, score):
        self.avg_score = float(self.avg_score * self.update_count + score) / float(self.update_count + 1)
        self.update_count += 1


from cost import cost, ssap_cost, ssap_cost_overbudget, ssap_multi_cost
from action import tolistMultiActionSequence, toActionMultiActionSequence, toActionActionSequence, tolistActionSequence, Action
import random
import copy
import sys
import numpy as np

# random rollout policy
def random_rollout(subsequence, action_set, total_budget):
    """
    total_budget : (total_stages, deploy_budget)
        Single robot budget
    """
    num_actions = len(action_set)
    if num_actions <= 0:
        raise ValueError('rollout: num_actions is ' + str(num_actions))
    sequence = copy.deepcopy(subsequence)
    stages_remaining = total_budget[0] - ssap_cost(sequence)[0]
    deploys_remaining = total_budget[1] - ssap_cost(sequence)[1]

    # Our randomness is done with choosing the stage to deploy. 
    # Can't be flipping deploys at each stage, since the probabilities are skewed

    pseudo_stage_count = 0
    pseudo_stages = range(stages_remaining)
    if not pseudo_stages:
        return sequence

    # print('Total Budget', total_budget)
    # print('Cost: ', ssap_cost(sequence))
    # print('Pseudo Stages: ', pseudo_stages)
    # print('Stages Remaining: ', stages_remaining)
    # print('')

    if deploys_remaining > len(pseudo_stages):
        deploys_remaining = len(pseudo_stages)

    deploy_stages = np.random.choice(pseudo_stages, size=deploys_remaining, replace=False)
    while not ssap_cost_overbudget(sequence, total_budget):
        if pseudo_stage_count in deploy_stages:
            sequence.append(action_set[1])
        else:
            sequence.append(action_set[0])
        pseudo_stage_count += 1

    return sequence

# SSAP rollout policy
def ssap_rollout(subsequence, action_set, obs_vals, total_budget, ssap):
    current_stage = len(subsequence)

    thresholds = ssap.thresholds
    # print('SSAP Rollout')
    # print('Rollout Budget: ', total_budget)
    sequence = copy.deepcopy(subsequence)
    # print('Sequence')
    # print(tolistActionSequence(sequence))
    # print('Overbudget?: ', ssap_cost_overbudget(sequence, total_budget))
    while not ssap_cost_overbudget(sequence, total_budget):

        stages_remaining = total_budget[0] - ssap_cost(sequence)[0]
        deploys_remaining = int(total_budget[1] - ssap_cost(sequence)[1])
        # print('Deploys Remaining: ', deploys_remaining)

        # TODO should this be >= ? Or did we get it right?
        if deploys_remaining > stages_remaining:
            # automatically deploy
            sequence.append(action_set[1])
            continue
        # print('Budget Cost: ', ssap_cost(sequence))
        # print('Stage: ', current_stage)

        stage_threshold = np.trim_zeros(thresholds[:,-current_stage], trim='b')
        # print('Stage Thresh: ', stage_threshold)
        # print('Deploys Remaining: ', deploys_remaining)
        thresh_val = stage_threshold[-(deploys_remaining+1)]
        current_obs_val = obs_vals[current_stage]
        # print('Thresh Val: ', thresh_val)
        # print('Current Obs: ', current_obs_val)
        # print('')
        if (current_obs_val > thresh_val) or (deploys_remaining == stages_remaining) :
            # deploy
            sequence.append(action_set[1])
        else:
            # don't deploy
            sequence.append(action_set[0])

        current_stage += 1 


    # pad with non-deploys
    while ssap_cost(sequence)[0] < total_budget[0] and ssap_cost(sequence)[1] == total_budget[1]:
        sequence.append(action_set[0])

    # print('Length: ', len(sequence))

    return sequence

# random multi rollout policy
def random_multi_rollout(subsequence, action_set, total_budget):
    """

    total_budget : (total_stages, np.array(deploy_budget))
        Tuple of budgets
    """
    num_actions = len(action_set)
    if num_actions <= 0:
        raise ValueError('rollout: num_actions is ' + str(num_actions))
    sequence = copy.deepcopy(subsequence)
    stages_remaining = total_budget[0] - len(subsequence)
    
    remaining_sequences = []
    random_deploys = []
    pseudo_stages = range(stages_remaining)
    if not pseudo_stages:
        return sequence


    total_rob_cost = ssap_multi_cost(subsequence)
    # each rob cost looks like (stage_count, deploy_count)
    # print('Total Budget: ', total_budget)
    # print('Total Rob Cost: ', total_rob_cost)
    # print(np.array(tolistMultiActionSequence(subsequence)))
    for i, rob in enumerate(total_rob_cost):
        remaining_deploys = total_budget[1][i] - rob[1]
        # print(i, remaining_deploys)
        if remaining_deploys > len(pseudo_stages):
            remaining_deploys = len(pseudo_stages)
        try:
            deploy_stages = np.random.choice(pseudo_stages, size=int(remaining_deploys), replace=False)
        except:
            print(np.array(tolistMultiActionSequence(subsequence)))
            # print('Total Budget: ', total_budget)
            print('Total Budget: ', total_budget[1][i])
            print('Rob ID: ', i)
            print('Rob Cost: ', rob[1])
            print('Remaining Deploys: ',remaining_deploys)
            raise ValueError()
        random_deploys.append(deploy_stages)


    num_robs = len(random_deploys)
    for i in range(stages_remaining):
        # thanks to Ian here, this reference to the same instance killed me
        # first, pad remaining stages with no deployment
        remaining_sequences.append(copy.copy(action_set[0]))
   
        old_deploy_action = np.array(remaining_sequences[i].deploy_actions)
        # we deploy at that stage if it was in the randomly chosen stage list
        for j in range(num_robs):
            if i in random_deploys[j]:
                old_deploy_action[j] = 1

            remaining_sequences[i].deploy_actions = tuple(old_deploy_action) 

    sequence = sequence + remaining_sequences

    if len(sequence) != total_budget[0]:
        raise ValueError('Error Different Sequence Length')
    # print('Rollout Sequence')
    # rollout_seq = np.stack(tolistMultiActionSequence(sequence), axis=0)
    # print(rollout_seq)

    return sequence

def ssap_adaptive_rollout(subsequence, action_set, obs_vals, total_budget, ssap, robot_id):
    current_stage = len(subsequence)
    threshold_pseudo_stage = np.sum(~np.isnan(ssap.real_obs[robot_id][:current_stage]))+1
    #  calculate number of false stages
    num_nan_stages = np.sum(np.isnan(ssap.real_obs[robot_id]))

    thresholds = ssap.adaptive_thresholds[robot_id]
    sequence = copy.deepcopy(subsequence)
    
    # print('Robot ID: ', robot_id)
    # print('Observations: ', obs_vals)
    # print('Initial Current Stage: ', current_stage)
    # print('Initial Thresh Stage: ', threshold_pseudo_stage)

    while not ssap_cost_overbudget(sequence, total_budget):
        stages_remaining = total_budget[0] - ssap_cost(sequence)[0] - num_nan_stages
        # stages_remaining = threshold_pseudo_stage - ssap_cost(sequence)[0]

        deploys_remaining = int(total_budget[1] - ssap_cost(sequence)[1])
        current_obs_val = obs_vals[current_stage]
        # print('Stages Remaining: ', stages_remaining)
        # print('Deploys Remaining: ', deploys_remaining)
        # print('Current Stage: ', current_stage)
        # print('Thresh Stage: ', threshold_pseudo_stage)        
        # print('Current Obs: ', current_obs_val)
        #  Handle those -9000 values
        if current_obs_val == -9000:
            # don't deploy due to Nan
            sequence.append(action_set[0])
            continue

        # TODO should this be >= ? Or did we get it right?
        if deploys_remaining > stages_remaining:
            # automatically deploy
            sequence.append(action_set[1])
            continue


        stage_threshold = np.trim_zeros(thresholds[:,-threshold_pseudo_stage], trim='b')
        # print(stage_threshold)
        # print(stages_remaining)
        # print(deploys_remaining)
        thresh_val = stage_threshold[-(deploys_remaining+1)]
        

        if (current_obs_val > thresh_val) or (deploys_remaining == stages_remaining):
            # deploy
            sequence.append(action_set[1])
        else:
            # don't deploy
            sequence.append(action_set[0])

        current_stage += 1
        threshold_pseudo_stage += 1 


    # pad with non-deploys
    while ssap_cost(sequence)[0] < total_budget[0] and ssap_cost(sequence)[1] == total_budget[1]:
        sequence.append(action_set[0])

    return sequence


def ssap_multi_rollout(subsequence, action_set, obs_vals, total_budget, ssap):
    """
    obs_vals : np.array (robot, stages)
        Multi array of observations

    total_budget : (total_stages, np.array(deploy_budget))
        Tuple of budgets

    """
    # print('Multi Rollout')
    multi_subsequence = []

    # turn subsequence into separate lists
    subseq_list = tolistMultiActionSequence(subsequence)

    single_action_set = []
    for i in range(2):
        id = i 
        single_action_set.append(Action(id,i))

    for i, rob_subseq in enumerate(subseq_list):
        # print('Robot SSAP Rollout ID: ', i)
        single_obs_val = obs_vals[i]
        single_budget = (total_budget[0], int(total_budget[1][i]))
        single_subseq = toActionActionSequence(rob_subseq)
        
        rolled_out_subsequence = ssap_adaptive_rollout(single_subseq, single_action_set, single_obs_val, single_budget, ssap, robot_id=i)
        rolled_out_sub_list = tolistActionSequence(rolled_out_subsequence)
        multi_subsequence.append(rolled_out_sub_list)
        # print('')

    return toActionMultiActionSequence(multi_subsequence)


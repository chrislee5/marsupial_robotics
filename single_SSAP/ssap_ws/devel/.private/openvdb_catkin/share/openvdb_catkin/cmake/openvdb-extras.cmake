# This overrides the dependency tracker with the OpenVDB library file.
set(OpenVDBLibraries "/home/chris/ssap_ws/devel/.private/openvdb_catkin/lib/libopenvdb${CMAKE_SHARED_LIBRARY_SUFFIX}")
set( openvdb_catkin_LIBRARIES ${OpenVDBLibraries} tbb Half boost_system boost_iostreams z)

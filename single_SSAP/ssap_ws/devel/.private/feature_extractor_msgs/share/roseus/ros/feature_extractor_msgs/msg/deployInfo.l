;; Auto-generated. Do not edit!


(when (boundp 'feature_extractor_msgs::deployInfo)
  (if (not (find-package "FEATURE_EXTRACTOR_MSGS"))
    (make-package "FEATURE_EXTRACTOR_MSGS"))
  (shadow 'deployInfo (find-package "FEATURE_EXTRACTOR_MSGS")))
(unless (find-package "FEATURE_EXTRACTOR_MSGS::DEPLOYINFO")
  (make-package "FEATURE_EXTRACTOR_MSGS::DEPLOYINFO"))

(in-package "ROS")
;;//! \htmlinclude deployInfo.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass feature_extractor_msgs::deployInfo
  :super ros::object
  :slots (_header _pose _stage_number _deploy_value _ssap_value ))

(defmethod feature_extractor_msgs::deployInfo
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:pose __pose) (instance geometry_msgs::Pose :init))
    ((:stage_number __stage_number) 0)
    ((:deploy_value __deploy_value) 0)
    ((:ssap_value __ssap_value) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _pose __pose)
   (setq _stage_number (round __stage_number))
   (setq _deploy_value (round __deploy_value))
   (setq _ssap_value (round __ssap_value))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:pose
   (&rest __pose)
   (if (keywordp (car __pose))
       (send* _pose __pose)
     (progn
       (if __pose (setq _pose (car __pose)))
       _pose)))
  (:stage_number
   (&optional __stage_number)
   (if __stage_number (setq _stage_number __stage_number)) _stage_number)
  (:deploy_value
   (&optional __deploy_value)
   (if __deploy_value (setq _deploy_value __deploy_value)) _deploy_value)
  (:ssap_value
   (&optional __ssap_value)
   (if __ssap_value (setq _ssap_value __ssap_value)) _ssap_value)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; geometry_msgs/Pose _pose
    (send _pose :serialization-length)
    ;; int16 _stage_number
    2
    ;; int32 _deploy_value
    4
    ;; int32 _ssap_value
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; geometry_msgs/Pose _pose
       (send _pose :serialize s)
     ;; int16 _stage_number
       (write-word _stage_number s)
     ;; int32 _deploy_value
       (write-long _deploy_value s)
     ;; int32 _ssap_value
       (write-long _ssap_value s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; geometry_msgs/Pose _pose
     (send _pose :deserialize buf ptr-) (incf ptr- (send _pose :serialization-length))
   ;; int16 _stage_number
     (setq _stage_number (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int32 _deploy_value
     (setq _deploy_value (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _ssap_value
     (setq _ssap_value (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get feature_extractor_msgs::deployInfo :md5sum-) "7eb038e5be29537bc00088a677917ad0")
(setf (get feature_extractor_msgs::deployInfo :datatype-) "feature_extractor_msgs/deployInfo")
(setf (get feature_extractor_msgs::deployInfo :definition-)
      "Header header

# robot position
geometry_msgs/Pose pose

# stage number
int16 stage_number

# deploy location value - frontier count
int32 deploy_value
int32 ssap_value
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

")



(provide :feature_extractor_msgs/deployInfo "7eb038e5be29537bc00088a677917ad0")



// Auto-generated. Do not edit!

// (in-package feature_extractor_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let geometry_msgs = _finder('geometry_msgs');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class deployInfo {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.pose = null;
      this.stage_number = null;
      this.deploy_value = null;
      this.ssap_value = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('pose')) {
        this.pose = initObj.pose
      }
      else {
        this.pose = new geometry_msgs.msg.Pose();
      }
      if (initObj.hasOwnProperty('stage_number')) {
        this.stage_number = initObj.stage_number
      }
      else {
        this.stage_number = 0;
      }
      if (initObj.hasOwnProperty('deploy_value')) {
        this.deploy_value = initObj.deploy_value
      }
      else {
        this.deploy_value = 0;
      }
      if (initObj.hasOwnProperty('ssap_value')) {
        this.ssap_value = initObj.ssap_value
      }
      else {
        this.ssap_value = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type deployInfo
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [pose]
    bufferOffset = geometry_msgs.msg.Pose.serialize(obj.pose, buffer, bufferOffset);
    // Serialize message field [stage_number]
    bufferOffset = _serializer.int16(obj.stage_number, buffer, bufferOffset);
    // Serialize message field [deploy_value]
    bufferOffset = _serializer.int32(obj.deploy_value, buffer, bufferOffset);
    // Serialize message field [ssap_value]
    bufferOffset = _serializer.int32(obj.ssap_value, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type deployInfo
    let len;
    let data = new deployInfo(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [pose]
    data.pose = geometry_msgs.msg.Pose.deserialize(buffer, bufferOffset);
    // Deserialize message field [stage_number]
    data.stage_number = _deserializer.int16(buffer, bufferOffset);
    // Deserialize message field [deploy_value]
    data.deploy_value = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [ssap_value]
    data.ssap_value = _deserializer.int32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 66;
  }

  static datatype() {
    // Returns string type for a message object
    return 'feature_extractor_msgs/deployInfo';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '7eb038e5be29537bc00088a677917ad0';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    # robot position
    geometry_msgs/Pose pose
    
    # stage number
    int16 stage_number
    
    # deploy location value - frontier count
    int32 deploy_value
    int32 ssap_value
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new deployInfo(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.pose !== undefined) {
      resolved.pose = geometry_msgs.msg.Pose.Resolve(msg.pose)
    }
    else {
      resolved.pose = new geometry_msgs.msg.Pose()
    }

    if (msg.stage_number !== undefined) {
      resolved.stage_number = msg.stage_number;
    }
    else {
      resolved.stage_number = 0
    }

    if (msg.deploy_value !== undefined) {
      resolved.deploy_value = msg.deploy_value;
    }
    else {
      resolved.deploy_value = 0
    }

    if (msg.ssap_value !== undefined) {
      resolved.ssap_value = msg.ssap_value;
    }
    else {
      resolved.ssap_value = 0
    }

    return resolved;
    }
};

module.exports = deployInfo;

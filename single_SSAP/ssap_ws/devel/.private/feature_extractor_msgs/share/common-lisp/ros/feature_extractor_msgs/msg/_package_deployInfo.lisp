(cl:in-package feature_extractor_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          POSE-VAL
          POSE
          STAGE_NUMBER-VAL
          STAGE_NUMBER
          DEPLOY_VALUE-VAL
          DEPLOY_VALUE
          SSAP_VALUE-VAL
          SSAP_VALUE
))
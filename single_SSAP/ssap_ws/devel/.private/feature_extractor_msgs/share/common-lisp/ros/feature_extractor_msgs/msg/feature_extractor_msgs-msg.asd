
(cl:in-package :asdf)

(defsystem "feature_extractor_msgs-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "deployInfo" :depends-on ("_package_deployInfo"))
    (:file "_package_deployInfo" :depends-on ("_package"))
  ))
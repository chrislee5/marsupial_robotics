; Auto-generated. Do not edit!


(cl:in-package feature_extractor_msgs-msg)


;//! \htmlinclude deployInfo.msg.html

(cl:defclass <deployInfo> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (pose
    :reader pose
    :initarg :pose
    :type geometry_msgs-msg:Pose
    :initform (cl:make-instance 'geometry_msgs-msg:Pose))
   (stage_number
    :reader stage_number
    :initarg :stage_number
    :type cl:fixnum
    :initform 0)
   (deploy_value
    :reader deploy_value
    :initarg :deploy_value
    :type cl:integer
    :initform 0)
   (ssap_value
    :reader ssap_value
    :initarg :ssap_value
    :type cl:integer
    :initform 0))
)

(cl:defclass deployInfo (<deployInfo>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <deployInfo>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'deployInfo)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name feature_extractor_msgs-msg:<deployInfo> is deprecated: use feature_extractor_msgs-msg:deployInfo instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <deployInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader feature_extractor_msgs-msg:header-val is deprecated.  Use feature_extractor_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'pose-val :lambda-list '(m))
(cl:defmethod pose-val ((m <deployInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader feature_extractor_msgs-msg:pose-val is deprecated.  Use feature_extractor_msgs-msg:pose instead.")
  (pose m))

(cl:ensure-generic-function 'stage_number-val :lambda-list '(m))
(cl:defmethod stage_number-val ((m <deployInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader feature_extractor_msgs-msg:stage_number-val is deprecated.  Use feature_extractor_msgs-msg:stage_number instead.")
  (stage_number m))

(cl:ensure-generic-function 'deploy_value-val :lambda-list '(m))
(cl:defmethod deploy_value-val ((m <deployInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader feature_extractor_msgs-msg:deploy_value-val is deprecated.  Use feature_extractor_msgs-msg:deploy_value instead.")
  (deploy_value m))

(cl:ensure-generic-function 'ssap_value-val :lambda-list '(m))
(cl:defmethod ssap_value-val ((m <deployInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader feature_extractor_msgs-msg:ssap_value-val is deprecated.  Use feature_extractor_msgs-msg:ssap_value instead.")
  (ssap_value m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <deployInfo>) ostream)
  "Serializes a message object of type '<deployInfo>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'pose) ostream)
  (cl:let* ((signed (cl:slot-value msg 'stage_number)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'deploy_value)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'ssap_value)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <deployInfo>) istream)
  "Deserializes a message object of type '<deployInfo>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'pose) istream)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'stage_number) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'deploy_value) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'ssap_value) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<deployInfo>)))
  "Returns string type for a message object of type '<deployInfo>"
  "feature_extractor_msgs/deployInfo")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'deployInfo)))
  "Returns string type for a message object of type 'deployInfo"
  "feature_extractor_msgs/deployInfo")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<deployInfo>)))
  "Returns md5sum for a message object of type '<deployInfo>"
  "7eb038e5be29537bc00088a677917ad0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'deployInfo)))
  "Returns md5sum for a message object of type 'deployInfo"
  "7eb038e5be29537bc00088a677917ad0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<deployInfo>)))
  "Returns full string definition for message of type '<deployInfo>"
  (cl:format cl:nil "Header header~%~%# robot position~%geometry_msgs/Pose pose~%~%# stage number~%int16 stage_number~%~%# deploy location value - frontier count~%int32 deploy_value~%int32 ssap_value~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'deployInfo)))
  "Returns full string definition for message of type 'deployInfo"
  (cl:format cl:nil "Header header~%~%# robot position~%geometry_msgs/Pose pose~%~%# stage number~%int16 stage_number~%~%# deploy location value - frontier count~%int32 deploy_value~%int32 ssap_value~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <deployInfo>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'pose))
     2
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <deployInfo>))
  "Converts a ROS message object to a list"
  (cl:list 'deployInfo
    (cl:cons ':header (header msg))
    (cl:cons ':pose (pose msg))
    (cl:cons ':stage_number (stage_number msg))
    (cl:cons ':deploy_value (deploy_value msg))
    (cl:cons ':ssap_value (ssap_value msg))
))

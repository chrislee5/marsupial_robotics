; Auto-generated. Do not edit!


(cl:in-package vdbmap-msg)


;//! \htmlinclude VDBMapStatus.msg.html

(cl:defclass <VDBMapStatus> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass VDBMapStatus (<VDBMapStatus>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <VDBMapStatus>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'VDBMapStatus)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name vdbmap-msg:<VDBMapStatus> is deprecated: use vdbmap-msg:VDBMapStatus instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <VDBMapStatus>) ostream)
  "Serializes a message object of type '<VDBMapStatus>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <VDBMapStatus>) istream)
  "Deserializes a message object of type '<VDBMapStatus>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<VDBMapStatus>)))
  "Returns string type for a message object of type '<VDBMapStatus>"
  "vdbmap/VDBMapStatus")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'VDBMapStatus)))
  "Returns string type for a message object of type 'VDBMapStatus"
  "vdbmap/VDBMapStatus")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<VDBMapStatus>)))
  "Returns md5sum for a message object of type '<VDBMapStatus>"
  "d41d8cd98f00b204e9800998ecf8427e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'VDBMapStatus)))
  "Returns md5sum for a message object of type 'VDBMapStatus"
  "d41d8cd98f00b204e9800998ecf8427e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<VDBMapStatus>)))
  "Returns full string definition for message of type '<VDBMapStatus>"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'VDBMapStatus)))
  "Returns full string definition for message of type 'VDBMapStatus"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <VDBMapStatus>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <VDBMapStatus>))
  "Converts a ROS message object to a list"
  (cl:list 'VDBMapStatus
))

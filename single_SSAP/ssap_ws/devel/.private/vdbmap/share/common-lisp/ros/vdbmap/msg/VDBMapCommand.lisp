; Auto-generated. Do not edit!


(cl:in-package vdbmap-msg)


;//! \htmlinclude VDBMapCommand.msg.html

(cl:defclass <VDBMapCommand> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass VDBMapCommand (<VDBMapCommand>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <VDBMapCommand>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'VDBMapCommand)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name vdbmap-msg:<VDBMapCommand> is deprecated: use vdbmap-msg:VDBMapCommand instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <VDBMapCommand>) ostream)
  "Serializes a message object of type '<VDBMapCommand>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <VDBMapCommand>) istream)
  "Deserializes a message object of type '<VDBMapCommand>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<VDBMapCommand>)))
  "Returns string type for a message object of type '<VDBMapCommand>"
  "vdbmap/VDBMapCommand")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'VDBMapCommand)))
  "Returns string type for a message object of type 'VDBMapCommand"
  "vdbmap/VDBMapCommand")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<VDBMapCommand>)))
  "Returns md5sum for a message object of type '<VDBMapCommand>"
  "d41d8cd98f00b204e9800998ecf8427e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'VDBMapCommand)))
  "Returns md5sum for a message object of type 'VDBMapCommand"
  "d41d8cd98f00b204e9800998ecf8427e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<VDBMapCommand>)))
  "Returns full string definition for message of type '<VDBMapCommand>"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'VDBMapCommand)))
  "Returns full string definition for message of type 'VDBMapCommand"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <VDBMapCommand>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <VDBMapCommand>))
  "Converts a ROS message object to a list"
  (cl:list 'VDBMapCommand
))


(cl:in-package :asdf)

(defsystem "vdbmap-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "VDBMapCommand" :depends-on ("_package_VDBMapCommand"))
    (:file "_package_VDBMapCommand" :depends-on ("_package"))
    (:file "VDBMapStatus" :depends-on ("_package_VDBMapStatus"))
    (:file "_package_VDBMapStatus" :depends-on ("_package"))
  ))
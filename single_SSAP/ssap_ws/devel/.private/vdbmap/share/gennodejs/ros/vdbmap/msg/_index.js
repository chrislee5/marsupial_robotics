
"use strict";

let VDBMapCommand = require('./VDBMapCommand.js');
let VDBMapStatus = require('./VDBMapStatus.js');

module.exports = {
  VDBMapCommand: VDBMapCommand,
  VDBMapStatus: VDBMapStatus,
};

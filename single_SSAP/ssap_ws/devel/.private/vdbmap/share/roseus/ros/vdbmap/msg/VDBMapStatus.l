;; Auto-generated. Do not edit!


(when (boundp 'vdbmap::VDBMapStatus)
  (if (not (find-package "VDBMAP"))
    (make-package "VDBMAP"))
  (shadow 'VDBMapStatus (find-package "VDBMAP")))
(unless (find-package "VDBMAP::VDBMAPSTATUS")
  (make-package "VDBMAP::VDBMAPSTATUS"))

(in-package "ROS")
;;//! \htmlinclude VDBMapStatus.msg.html


(defclass vdbmap::VDBMapStatus
  :super ros::object
  :slots ())

(defmethod vdbmap::VDBMapStatus
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(setf (get vdbmap::VDBMapStatus :md5sum-) "d41d8cd98f00b204e9800998ecf8427e")
(setf (get vdbmap::VDBMapStatus :datatype-) "vdbmap/VDBMapStatus")
(setf (get vdbmap::VDBMapStatus :definition-)
      "
")



(provide :vdbmap/VDBMapStatus "d41d8cd98f00b204e9800998ecf8427e")



;; Auto-generated. Do not edit!


(when (boundp 'vdbmap::VDBMapCommand)
  (if (not (find-package "VDBMAP"))
    (make-package "VDBMAP"))
  (shadow 'VDBMapCommand (find-package "VDBMAP")))
(unless (find-package "VDBMAP::VDBMAPCOMMAND")
  (make-package "VDBMAP::VDBMAPCOMMAND"))

(in-package "ROS")
;;//! \htmlinclude VDBMapCommand.msg.html


(defclass vdbmap::VDBMapCommand
  :super ros::object
  :slots ())

(defmethod vdbmap::VDBMapCommand
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(setf (get vdbmap::VDBMapCommand :md5sum-) "d41d8cd98f00b204e9800998ecf8427e")
(setf (get vdbmap::VDBMapCommand :datatype-) "vdbmap/VDBMapCommand")
(setf (get vdbmap::VDBMapCommand :definition-)
      "
")



(provide :vdbmap/VDBMapCommand "d41d8cd98f00b204e9800998ecf8427e")



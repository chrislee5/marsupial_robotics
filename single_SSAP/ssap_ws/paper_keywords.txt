Multi-Robot Systems
Path Planning for Multiple Mobile Robots or Agents (downvote due to niche community focusing on path planning in the theoretical)
Mapping (would go to SLAM people)

Task and Motion Planning (goes to people who also care about motion planning rather than purely tasks, which is more like scheduling)
Motion and Path Planning
Distributed Robot Systems (swarm robotic people, so not quite what we want here)

Agent Based Systems (more AI based stuff)
Aerial Systems: Applications (a lot of focus on controls)
Cooperating Robots (not sure which community this would head towards)

Graeme's ====================================

Multi-Robot Systems
Task and Motion Planning

Cooperating Robots
Path Planning for Multiple Mobile Robots or Agents
Motion and Path Planning
Task Planning
Planning, Scheduling and Coordination
Field Robots (no real experiments here)

Environment Monitoring and Management
Robotics in Hazardous Fields
Search and Rescue Robots
Autonomous Agents
Reactive and Sensor-Based Planning
Semantic Scene Understanding
Perception-Action Coupling
Aerial Systems: Perception and Autonomy
Mining Robotics

Together ====================================

1. Multi-Robot Systems
2. Task and Motion Planning 


# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "".split(';') if "" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;nav_msgs;geometry_msgs;visualization_msgs;feature_extractor_msgs;std_msgs;pcl_ros;vdb_utils;vdbmap_interface".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "feature_extractor"
PROJECT_SPACE_DIR = "/home/chris/ssap_ws/install"
PROJECT_VERSION = "0.0.0"

set(_CATKIN_CURRENT_PACKAGE "feature_extractor")
set(feature_extractor_VERSION "0.0.0")
set(feature_extractor_MAINTAINER "chris <chris@todo.todo>")
set(feature_extractor_PACKAGE_FORMAT "2")
set(feature_extractor_BUILD_DEPENDS "roscpp" "std_msgs" "base" "nav_msgs" "geometry_msgs" "visualization_msgs" "feature_extractor_msgs" "pcl_ros" "vdb_utils" "vdbmap_interface" "sensor_msgs")
set(feature_extractor_BUILD_EXPORT_DEPENDS "roscpp" "std_msgs" "base" "pcl_ros" "vdb_utils" "vdbmap_interface" "sensor_msgs")
set(feature_extractor_BUILDTOOL_DEPENDS "catkin")
set(feature_extractor_BUILDTOOL_EXPORT_DEPENDS )
set(feature_extractor_EXEC_DEPENDS "roscpp" "std_msgs" "base" "nav_msgs" "geometry_msgs" "visualization_msgs" "feature_extractor_msgs" "pcl_ros" "vdb_utils" "vdbmap_interface" "sensor_msgs")
set(feature_extractor_RUN_DEPENDS "roscpp" "std_msgs" "base" "nav_msgs" "geometry_msgs" "visualization_msgs" "feature_extractor_msgs" "pcl_ros" "vdb_utils" "vdbmap_interface" "sensor_msgs")
set(feature_extractor_TEST_DEPENDS )
set(feature_extractor_DOC_DEPENDS )
set(feature_extractor_URL_WEBSITE "")
set(feature_extractor_URL_BUGTRACKER "")
set(feature_extractor_URL_REPOSITORY "")
set(feature_extractor_DEPRECATED "")
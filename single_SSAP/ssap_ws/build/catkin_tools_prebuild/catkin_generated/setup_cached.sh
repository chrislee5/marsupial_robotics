#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/chris/ssap_ws/devel/.private/catkin_tools_prebuild:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/chris/subt_ws/devel/lib:/opt/ros/melodic/lib"
export PKG_CONFIG_PATH="/home/chris/subt_ws/devel/lib/pkgconfig:/opt/ros/melodic/lib/pkgconfig"
export PWD="/home/chris/ssap_ws/build/catkin_tools_prebuild"
export PYTHONPATH="/home/chris/subt_ws/devel/lib/python2.7/dist-packages:/opt/ros/melodic/lib/python2.7/dist-packages"
export ROSLISP_PACKAGE_DIRECTORIES="/home/chris/ssap_ws/devel/.private/catkin_tools_prebuild/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/chris/ssap_ws/build/catkin_tools_prebuild:$ROS_PACKAGE_PATH"
#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/chris/ssap_ws/devel/.private/vdbmap:$CMAKE_PREFIX_PATH"
export PWD="/home/chris/ssap_ws/build/vdbmap"
export ROSLISP_PACKAGE_DIRECTORIES="/home/chris/ssap_ws/devel/.private/vdbmap/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/chris/ssap_ws/src/vdbmap:$ROS_PACKAGE_PATH"
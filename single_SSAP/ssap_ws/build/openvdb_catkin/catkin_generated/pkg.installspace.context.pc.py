# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lopenvdb_catkin".split(';') if "-lopenvdb_catkin" != "" else []
PROJECT_NAME = "openvdb_catkin"
PROJECT_SPACE_DIR = "/home/chris/ssap_ws/install"
PROJECT_VERSION = "0.0.0"

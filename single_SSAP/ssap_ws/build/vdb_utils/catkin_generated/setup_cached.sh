#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/chris/ssap_ws/devel/.private/vdb_utils:$CMAKE_PREFIX_PATH"
export PWD="/home/chris/ssap_ws/build/vdb_utils"
export ROSLISP_PACKAGE_DIRECTORIES="/home/chris/ssap_ws/devel/.private/vdb_utils/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/chris/ssap_ws/src/vdb_utils:$ROS_PACKAGE_PATH"
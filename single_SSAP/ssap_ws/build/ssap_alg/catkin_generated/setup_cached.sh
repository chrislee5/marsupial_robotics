#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/chris/ssap_ws/devel/.private/ssap_alg:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/chris/ssap_ws/devel/.private/ssap_alg/lib:$LD_LIBRARY_PATH"
export PWD="/home/chris/ssap_ws/build/ssap_alg"
export PYTHONPATH="/home/chris/ssap_ws/devel/.private/ssap_alg/lib/python2.7/dist-packages:/home/chris/ssap_ws/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/chris/ssap_ws/devel/.private/ssap_alg/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/chris/ssap_ws/src/ssap_alg:$ROS_PACKAGE_PATH"
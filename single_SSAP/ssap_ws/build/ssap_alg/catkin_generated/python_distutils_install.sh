#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/chris/ssap_ws/src/ssap_alg"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/chris/ssap_ws/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/chris/ssap_ws/install/lib/python2.7/dist-packages:/home/chris/ssap_ws/build/ssap_alg/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/chris/ssap_ws/build/ssap_alg" \
    "/usr/bin/python2" \
    "/home/chris/ssap_ws/src/ssap_alg/setup.py" \
     \
    build --build-base "/home/chris/ssap_ws/build/ssap_alg" \
    install \
    --root="${DESTDIR-/}" \
    --install-layout=deb --prefix="/home/chris/ssap_ws/install" --install-scripts="/home/chris/ssap_ws/install/bin"

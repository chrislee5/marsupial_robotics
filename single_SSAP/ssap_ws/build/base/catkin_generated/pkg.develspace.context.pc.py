# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/chris/ssap_ws/src/base_main_class/include".split(';') if "/home/chris/ssap_ws/src/base_main_class/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;std_msgs;nodelet".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lbase_node;-lbase_nodelet".split(';') if "-lbase_node;-lbase_nodelet" != "" else []
PROJECT_NAME = "base"
PROJECT_SPACE_DIR = "/home/chris/ssap_ws/devel/.private/base"
PROJECT_VERSION = "0.0.0"

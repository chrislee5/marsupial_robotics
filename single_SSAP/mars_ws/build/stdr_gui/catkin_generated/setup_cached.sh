#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_gui:$CMAKE_PREFIX_PATH"
export PWD="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_gui"
export ROSLISP_PACKAGE_DIRECTORIES="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_gui/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_gui:$ROS_PACKAGE_PATH"
# CMake generated Testfile for 
# Source directory: /home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_server
# Build directory: /home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_server
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("test")

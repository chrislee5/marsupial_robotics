# CMake generated Testfile for 
# Source directory: /home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_server/test
# Build directory: /home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_server/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_stdr_server_rostest_test_functional_interfaces_test.launch "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_server/catkin_generated/env_cached.sh" "/usr/bin/python2" "/opt/ros/melodic/share/catkin/cmake/test/run_tests.py" "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_server/test_results/stdr_server/rostest-test_functional_interfaces_test.xml" "--return-code" "/opt/ros/melodic/share/rostest/cmake/../../../bin/rostest --pkgdir=/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_server --package=stdr_server --results-filename test_functional_interfaces_test.xml --results-base-dir \"/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_server/test_results\" /home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_server/test/functional/interfaces_test.launch ")

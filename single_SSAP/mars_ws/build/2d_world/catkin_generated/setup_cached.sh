#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/2d_world:$CMAKE_PREFIX_PATH"
export PWD="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/2d_world"
export ROSLISP_PACKAGE_DIRECTORIES="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/2d_world/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/2d_world:$ROS_PACKAGE_PATH"
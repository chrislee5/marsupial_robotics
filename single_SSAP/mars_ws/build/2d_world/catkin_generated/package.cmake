set(_CATKIN_CURRENT_PACKAGE "2d_world")
set(2d_world_VERSION "0.0.0")
set(2d_world_MAINTAINER "subt-nuc-01 <subt-nuc-01@todo.todo>")
set(2d_world_PACKAGE_FORMAT "2")
set(2d_world_BUILD_DEPENDS "rospy" "std_msgs")
set(2d_world_BUILD_EXPORT_DEPENDS "rospy" "std_msgs")
set(2d_world_BUILDTOOL_DEPENDS "catkin")
set(2d_world_BUILDTOOL_EXPORT_DEPENDS )
set(2d_world_EXEC_DEPENDS "rospy" "std_msgs")
set(2d_world_RUN_DEPENDS "rospy" "std_msgs")
set(2d_world_TEST_DEPENDS )
set(2d_world_DOC_DEPENDS )
set(2d_world_URL_WEBSITE "")
set(2d_world_URL_BUGTRACKER "")
set(2d_world_URL_REPOSITORY "")
set(2d_world_DEPRECATED "")
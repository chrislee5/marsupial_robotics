# Install script for directory: /home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  
      if (NOT EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}")
        file(MAKE_DIRECTORY "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}")
      endif()
      if (NOT EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/.catkin")
        file(WRITE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/.catkin" "")
      endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install/_setup_util.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install" TYPE PROGRAM FILES "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/_setup_util.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install/env.sh")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install" TYPE PROGRAM FILES "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/env.sh")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install/setup.bash;/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install/local_setup.bash")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install" TYPE FILE FILES
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/setup.bash"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/local_setup.bash"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install/setup.sh;/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install/local_setup.sh")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install" TYPE FILE FILES
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/setup.sh"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/local_setup.sh"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install/setup.zsh;/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install/local_setup.zsh")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install" TYPE FILE FILES
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/setup.zsh"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/local_setup.zsh"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install/.rosinstall")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install" TYPE FILE FILES "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/.rosinstall")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stdr_msgs/msg" TYPE FILE FILES
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/Noise.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/LaserSensorMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/SonarSensorMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/KinematicMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/FootprintMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/RobotMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/RobotIndexedMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/RobotIndexedVectorMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/RfidSensorMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/RfidSensorMeasurementMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/RfidTag.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/RfidTagVector.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/SoundSensorMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/SoundSensorMeasurementMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/SoundSource.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/SoundSourceVector.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/ThermalSensorMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/ThermalSensorMeasurementMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/ThermalSource.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/ThermalSourceVector.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/CO2SensorMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/CO2SensorMeasurementMsg.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/CO2Source.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/msg/CO2SourceVector.msg"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stdr_msgs/srv" TYPE FILE FILES
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/srv/LoadMap.srv"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/srv/LoadExternalMap.srv"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/srv/RegisterGui.srv"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/srv/MoveRobot.srv"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/srv/AddRfidTag.srv"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/srv/DeleteRfidTag.srv"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/srv/AddThermalSource.srv"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/srv/DeleteThermalSource.srv"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/srv/AddSoundSource.srv"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/srv/DeleteSoundSource.srv"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/srv/AddCO2Source.srv"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/srv/DeleteCO2Source.srv"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stdr_msgs/action" TYPE FILE FILES
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/action/RegisterRobot.action"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/action/SpawnRobot.action"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/action/DeleteRobot.action"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stdr_msgs/msg" TYPE FILE FILES
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/RegisterRobotAction.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/RegisterRobotActionGoal.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/RegisterRobotActionResult.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/RegisterRobotActionFeedback.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/RegisterRobotGoal.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/RegisterRobotResult.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/RegisterRobotFeedback.msg"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stdr_msgs/msg" TYPE FILE FILES
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/SpawnRobotAction.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/SpawnRobotActionGoal.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/SpawnRobotActionResult.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/SpawnRobotActionFeedback.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/SpawnRobotGoal.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/SpawnRobotResult.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/SpawnRobotFeedback.msg"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stdr_msgs/msg" TYPE FILE FILES
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/DeleteRobotAction.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/DeleteRobotActionGoal.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/DeleteRobotActionResult.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/DeleteRobotActionFeedback.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/DeleteRobotGoal.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/DeleteRobotResult.msg"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/stdr_msgs/msg/DeleteRobotFeedback.msg"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stdr_msgs/cmake" TYPE FILE FILES "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/stdr_msgs-msg-paths.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/include/stdr_msgs")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/roseus/ros" TYPE DIRECTORY FILES "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/roseus/ros/stdr_msgs")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/common-lisp/ros/stdr_msgs")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gennodejs/ros" TYPE DIRECTORY FILES "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/share/gennodejs/ros/stdr_msgs")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process(COMMAND "/usr/bin/python2" -m compileall "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/lib/python2.7/dist-packages/stdr_msgs")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/lib/python2.7/dist-packages/stdr_msgs")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/stdr_msgs.pc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stdr_msgs/cmake" TYPE FILE FILES "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/stdr_msgs-msg-extras.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stdr_msgs/cmake" TYPE FILE FILES
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/stdr_msgsConfig.cmake"
    "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/catkin_generated/installspace/stdr_msgsConfig-version.cmake"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stdr_msgs" TYPE FILE FILES "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/package.xml")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/stdr_msgs" TYPE DIRECTORY FILES "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_msgs/include/" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/gtest/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_msgs/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")

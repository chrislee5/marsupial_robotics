#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/catkin_tools_prebuild:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/opt/ros/melodic/lib"
export PWD="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/catkin_tools_prebuild"
export ROSLISP_PACKAGE_DIRECTORIES="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/catkin_tools_prebuild/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/catkin_tools_prebuild:/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/2d_world:/opt/ros/melodic/share"
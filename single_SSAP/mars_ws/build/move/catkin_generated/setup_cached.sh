#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/move:$CMAKE_PREFIX_PATH"
export PWD="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/move"
export ROSLISP_PACKAGE_DIRECTORIES="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/move/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/move:$ROS_PACKAGE_PATH"
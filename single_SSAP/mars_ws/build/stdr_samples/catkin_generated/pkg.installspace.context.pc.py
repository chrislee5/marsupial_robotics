# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install/include".split(';') if "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roslib;roscpp;tf;stdr_msgs;geometry_msgs;sensor_msgs;nav_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "stdr_samples"
PROJECT_SPACE_DIR = "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/install"
PROJECT_VERSION = "0.3.2"

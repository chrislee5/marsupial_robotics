# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_samples/src/obstacle_avoidance/main.cpp" "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_samples/CMakeFiles/stdr_obstacle_avoidance.dir/src/obstacle_avoidance/main.cpp.o"
  "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_samples/src/obstacle_avoidance/obstacle_avoidance.cpp" "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/build/stdr_samples/CMakeFiles/stdr_obstacle_avoidance.dir/src/obstacle_avoidance/obstacle_avoidance.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"stdr_samples\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/src/stdr_simulator/stdr_samples/include"
  "/home/subt-nuc-01/grad_hw/marsupial_robotics/mars_ws/devel/.private/stdr_msgs/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

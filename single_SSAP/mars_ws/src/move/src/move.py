#!/usr/bin/env python

# Adds the necessary core ROS libraries for Python
import rospy

# The message used to specify velocity
from geometry_msgs.msg import Twist

def proc_scan():
    """Read the scan data and do something

    """

    print("Laser Blazer")

if __name__ == '__main__':
    rospy.init_node('move')

    # A publisher for the move data
    pub = rospy.Publisher('cmd_vel_mux/input/teleop', Twist, queue_size=1)

    rospy.Subscriber('scan',LaserScan,callback=proc_scan)

    # Drive forward at a given speed.  The x-axis for the turlebot is "forward" relative to the direction the robot is facing. The robot turns by rotating on its z-axis
    # Ungraded exercise: what units do these velocities use?
    # Ungraded exercise: why are the other linear or angular velocities not relevant for the turtlebot?
    command = Twist()
    command.linear.x = 0.1
    command.linear.y = 0.0
    command.linear.z = 0.0
    command.angular.x = 0.0
    command.angular.y = 0.0
    command.angular.z = 0.0

    # Loop at 10Hz, publishing movement commands until we shut down.
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        pub.publish(command)
        rate.sleep()



"use strict";

let AddThermalSource = require('./AddThermalSource.js')
let DeleteSoundSource = require('./DeleteSoundSource.js')
let DeleteRfidTag = require('./DeleteRfidTag.js')
let MoveRobot = require('./MoveRobot.js')
let AddCO2Source = require('./AddCO2Source.js')
let AddSoundSource = require('./AddSoundSource.js')
let DeleteThermalSource = require('./DeleteThermalSource.js')
let LoadExternalMap = require('./LoadExternalMap.js')
let AddRfidTag = require('./AddRfidTag.js')
let DeleteCO2Source = require('./DeleteCO2Source.js')
let RegisterGui = require('./RegisterGui.js')
let LoadMap = require('./LoadMap.js')

module.exports = {
  AddThermalSource: AddThermalSource,
  DeleteSoundSource: DeleteSoundSource,
  DeleteRfidTag: DeleteRfidTag,
  MoveRobot: MoveRobot,
  AddCO2Source: AddCO2Source,
  AddSoundSource: AddSoundSource,
  DeleteThermalSource: DeleteThermalSource,
  LoadExternalMap: LoadExternalMap,
  AddRfidTag: AddRfidTag,
  DeleteCO2Source: DeleteCO2Source,
  RegisterGui: RegisterGui,
  LoadMap: LoadMap,
};

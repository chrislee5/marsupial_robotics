
"use strict";

let Noise = require('./Noise.js');
let SoundSensorMsg = require('./SoundSensorMsg.js');
let SoundSourceVector = require('./SoundSourceVector.js');
let SonarSensorMsg = require('./SonarSensorMsg.js');
let ThermalSourceVector = require('./ThermalSourceVector.js');
let CO2SourceVector = require('./CO2SourceVector.js');
let SoundSource = require('./SoundSource.js');
let RfidTagVector = require('./RfidTagVector.js');
let RfidTag = require('./RfidTag.js');
let ThermalSensorMeasurementMsg = require('./ThermalSensorMeasurementMsg.js');
let RfidSensorMsg = require('./RfidSensorMsg.js');
let RobotMsg = require('./RobotMsg.js');
let ThermalSource = require('./ThermalSource.js');
let RfidSensorMeasurementMsg = require('./RfidSensorMeasurementMsg.js');
let CO2Source = require('./CO2Source.js');
let RobotIndexedMsg = require('./RobotIndexedMsg.js');
let ThermalSensorMsg = require('./ThermalSensorMsg.js');
let LaserSensorMsg = require('./LaserSensorMsg.js');
let FootprintMsg = require('./FootprintMsg.js');
let SoundSensorMeasurementMsg = require('./SoundSensorMeasurementMsg.js');
let CO2SensorMeasurementMsg = require('./CO2SensorMeasurementMsg.js');
let RobotIndexedVectorMsg = require('./RobotIndexedVectorMsg.js');
let KinematicMsg = require('./KinematicMsg.js');
let CO2SensorMsg = require('./CO2SensorMsg.js');
let DeleteRobotActionGoal = require('./DeleteRobotActionGoal.js');
let RegisterRobotAction = require('./RegisterRobotAction.js');
let SpawnRobotFeedback = require('./SpawnRobotFeedback.js');
let RegisterRobotActionFeedback = require('./RegisterRobotActionFeedback.js');
let SpawnRobotAction = require('./SpawnRobotAction.js');
let RegisterRobotFeedback = require('./RegisterRobotFeedback.js');
let RegisterRobotActionGoal = require('./RegisterRobotActionGoal.js');
let SpawnRobotResult = require('./SpawnRobotResult.js');
let DeleteRobotFeedback = require('./DeleteRobotFeedback.js');
let SpawnRobotGoal = require('./SpawnRobotGoal.js');
let RegisterRobotGoal = require('./RegisterRobotGoal.js');
let SpawnRobotActionResult = require('./SpawnRobotActionResult.js');
let DeleteRobotActionResult = require('./DeleteRobotActionResult.js');
let SpawnRobotActionGoal = require('./SpawnRobotActionGoal.js');
let DeleteRobotAction = require('./DeleteRobotAction.js');
let DeleteRobotGoal = require('./DeleteRobotGoal.js');
let RegisterRobotResult = require('./RegisterRobotResult.js');
let DeleteRobotResult = require('./DeleteRobotResult.js');
let SpawnRobotActionFeedback = require('./SpawnRobotActionFeedback.js');
let RegisterRobotActionResult = require('./RegisterRobotActionResult.js');
let DeleteRobotActionFeedback = require('./DeleteRobotActionFeedback.js');

module.exports = {
  Noise: Noise,
  SoundSensorMsg: SoundSensorMsg,
  SoundSourceVector: SoundSourceVector,
  SonarSensorMsg: SonarSensorMsg,
  ThermalSourceVector: ThermalSourceVector,
  CO2SourceVector: CO2SourceVector,
  SoundSource: SoundSource,
  RfidTagVector: RfidTagVector,
  RfidTag: RfidTag,
  ThermalSensorMeasurementMsg: ThermalSensorMeasurementMsg,
  RfidSensorMsg: RfidSensorMsg,
  RobotMsg: RobotMsg,
  ThermalSource: ThermalSource,
  RfidSensorMeasurementMsg: RfidSensorMeasurementMsg,
  CO2Source: CO2Source,
  RobotIndexedMsg: RobotIndexedMsg,
  ThermalSensorMsg: ThermalSensorMsg,
  LaserSensorMsg: LaserSensorMsg,
  FootprintMsg: FootprintMsg,
  SoundSensorMeasurementMsg: SoundSensorMeasurementMsg,
  CO2SensorMeasurementMsg: CO2SensorMeasurementMsg,
  RobotIndexedVectorMsg: RobotIndexedVectorMsg,
  KinematicMsg: KinematicMsg,
  CO2SensorMsg: CO2SensorMsg,
  DeleteRobotActionGoal: DeleteRobotActionGoal,
  RegisterRobotAction: RegisterRobotAction,
  SpawnRobotFeedback: SpawnRobotFeedback,
  RegisterRobotActionFeedback: RegisterRobotActionFeedback,
  SpawnRobotAction: SpawnRobotAction,
  RegisterRobotFeedback: RegisterRobotFeedback,
  RegisterRobotActionGoal: RegisterRobotActionGoal,
  SpawnRobotResult: SpawnRobotResult,
  DeleteRobotFeedback: DeleteRobotFeedback,
  SpawnRobotGoal: SpawnRobotGoal,
  RegisterRobotGoal: RegisterRobotGoal,
  SpawnRobotActionResult: SpawnRobotActionResult,
  DeleteRobotActionResult: DeleteRobotActionResult,
  SpawnRobotActionGoal: SpawnRobotActionGoal,
  DeleteRobotAction: DeleteRobotAction,
  DeleteRobotGoal: DeleteRobotGoal,
  RegisterRobotResult: RegisterRobotResult,
  DeleteRobotResult: DeleteRobotResult,
  SpawnRobotActionFeedback: SpawnRobotActionFeedback,
  RegisterRobotActionResult: RegisterRobotActionResult,
  DeleteRobotActionFeedback: DeleteRobotActionFeedback,
};
